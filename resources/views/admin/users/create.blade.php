@extends('admin.layout.master')                
@section('main_content')
<!-- Page Content -->
<div id="page-wrapper">
<div class="container-fluid">
<div class="row bg-title">
   <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title">{{$page_title or ''}}</h4>
   </div>
   <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
      <ol class="breadcrumb">
         <li><a href="{{url('/')}}/admin/dashboard">Dashboard</a></li>
         <li><a href="{{$module_url_path}}">{{$module_title or ''}}</a></li>
         <li class="active">Create User</li>
      </ol>
   </div>
   <!-- /.col-lg-12 -->
</div>
<!-- BEGIN Main Content -->
<div class="row">
   <div class="col-sm-12">
      <div class="white-box">
         {{-- @include('admin.layout._operation_status') --}}
         <div class="row">
            <div class="col-sm-12 col-xs-12">
               {!! Form::open([ 
               'method'=>'POST',
               'enctype' =>'multipart/form-data',   
               'class'=>'form-horizontal', 
               'id'=>'validation-form-user' 
               ]) !!} 
               {{ csrf_field() }}
               <div class="form-group row">
                  <label class="col-2 col-form-label">Firstname<i class="red">*</i></label>
                  <div class="col-10">
                     <input type="text" class="form-control" name="first_name" placeholder="Enter Firstname" data-parsley-required="true" value="{{ old('first_name') }}" />
                     <span class="red">{{ $errors->first('first_name') }}</span>
                  </div>
               </div>
               <div class="form-group row">
                  <label class="col-2 col-form-label">Lastname<i class="red">*</i></label>
                  <div class="col-10">
                     <input type="text" class="form-control" name="last_name" placeholder="Enter Lastname" data-parsley-required="true" value="{{ old('last_name') }}" />
                     <span class="red">{{ $errors->first('last_name') }}</span>
                  </div>
               </div>
               <div class="form-group row">
                  <label class="col-2 col-form-label">Email<i class="red">*</i></label>
                  <div class="col-10">
                     <input type="text" class="form-control" name="email" placeholder="Enter Email" data-parsley-required="true" data-parsley-type="email" value="{{ old('email') }}" />
                     <span class="red">{{ $errors->first('email') }}</span>
                  </div>
               </div>
               <div class="form-group row">
                  <label class="col-2 col-form-label">Phone<i class="red">*</i></label>
                  <div class="col-10">
                     <input type="number" class="form-control" name="phone" placeholder="Enter Phone No" data-parsley-required="true" data-parsley-length="[6, 16]" value="{{ old('phone') }}" />
                     <span class="red">{{ $errors->first('phone') }}</span>
                  </div>
               </div>
               <div class="form-group row">
                  <label class="col-2 col-form-label">Password<i class="red">*</i></label>
                  <div class="col-10">
                     <input type="password" class="form-control" name="password" placeholder="Enter password" data-parsley-required="true" data-parsley-length="[6, 16]" value="{{ old('password') }}" />
                     <span class="red">{{ $errors->first('password') }}</span>
                  </div>
               </div>
               <div class="form-group row">
                  <label class="col-2 col-form-label">Country<i class="red">*</i></label>
                  <div class="col-10">
                    {{--  <select class="form-control"  name="country" data-parsley-required="true" onchange="changeCountryRestriction(this)" >
                        <option value="">--Select Country--</option>
                        @if(isset($arr_country) && count($arr_country)>0)
                        @foreach($arr_country as $key => $value)
                        <option value="{{ $value['country_code'] }}">{{ $value['country_name'] }}</option>
                        @endforeach
                        @endif
                     </select>
 --}}
                                                 {{-- {{dd($countriesValues['name'])}} --}}

                     <select name="country" required="" id="country" onchange="getStates()" class="form-control">
                                          
                                           <option selected disabled>--select--</option>
                                                @foreach($countries as $countriesKey => $countriesValues)

                                                 
                                                   <option class="form-control" value="{{$countriesValues['id']}}"  >{{$countriesValues['country_name']}}</option>
                                        

                                                @endforeach
                                           </select>
                     <span class="red">{{ $errors->first('role') }}</span>
                  </div>
               </div>
              {{--  <div class="form-group row">
                  <label class="col-2 col-form-label">Street Address<i class="red">*</i></label>
                  <div class="col-10">
                     <input type="text" class="form-control" name="street_address" placeholder="Enter Street Address" data-parsley-required="true"  value="{{ old('street_address') }}" />
                     <span class="red">{{ $errors->first('street_address') }}</span>
                  </div>
               </div> --}}
                <div class="form-group row">
                  <label class="col-2 col-form-label">Street Address<i class="red">*</i></label>
                  <div class="col-10" >
                      {{-- {!! Form::text('street_address',['class'=>'form-control','data-parsley-required'=>'true', 'placeholder'=>'Enter Street Address', 'id'=>"autocomplete"]) !!} --}}
                        <input type="text" name="street_address" class="form-control" placeholder="Enter Street Address" id="autocomplete" data-parsley-type="number">
                      <span class="help-block">{{ $errors->first('street_address') }}</span>
                  </div>
            </div>

               <div class="form-group row">
                  <label class="col-2 col-form-label">State</label>
                  <div class="col-10">
                    {{--  <input type="text" class="form-control" name="state" placeholder="State" id="administrative_area_level_1" readonly=""  value="{{ old('state') }}" /> --}}
                    <select name="state" required="" id="state" class="form-control">
                    </select>
                     <span class="red">{{ $errors->first('state') }}</span>
                  </div>
               </div>
              {{--  <div class="form-group row">
                  <label class="col-2 col-form-label">City</label>
                  <div class="col-10">
                     <input type="text" class="form-control" name="city" placeholder="City" id="administrative_area_level_1" id="locality" readonly=""  value="{{ old('city') }}" />
                     <span class="red">{{ $errors->first('city') }}</span>
                  </div>
               </div> --}}
              {{--  <div class="form-group row">
                  <label class="col-2 col-form-label">Zip Code</label>
                  <div class="col-10">
                     <input type="text" class="form-control" name="zipcode" id="postal_code" placeholder="Enter Zip Code" data-parsley-length="[4, 12]" required="" value="{{ old('zipcode') }}" />
                     <span class="red">{{ $errors->first('zipcode') }}</span>
                  </div>
               </div> --}}
                <div class="form-group row">
                  <label class="col-2 col-form-label">Zip Code</label>
                  <div class="col-10" >

                     <input type="text" name="zipcode" class="form-control" placeholder="Zip code" id="postal_code" data-parsley-type="number" data-parsley-length="[4,12]">
                      <span class="help-block">{{ $errors->first('zipcode') }}</span>
                  </div>
            </div>
               <div class="form-group row">
                  <label class="col-2 col-form-label" for="ad_image">Profile Image <i class="red">*</i></label>
                  <div class="col-10">
                     <input type="file" name="profile_image" id="ad_image" class="dropify" data-default-file="{{url('/')}}/uploads/default.jpeg" required="" />
                  </div>
               </div>
               <span>{{ $errors->first('profile_image') }}</span>
               <div class="form-group row">
                  <div class="col-10">
                   {{--   <button type="submit" class="btn btn-success waves-effect waves-light m-r-10" value="Save">Save</button> --}}
                     <button type="button" class="btn btn-success waves-effect waves-light m-r-10" id="btn_add" value="Save">Save</button>

                        <a class="btn btn-inverse waves-effect waves-light" href="{{$module_url_path}}">Back</a>
                  </div>
               </div>
               {!! Form::close() !!}
            </div>
         </div>
      </div>
   </div>
</div>
<!-- END Main Content -->
<script src="https://maps.googleapis.com/maps/api/js?v=3&key=AIzaSyCccvQtzVx4aAt05YnfzJDSWEzPiVnNVsY&libraries=places&callback=initAutocomplete"
   async defer></script>
<script>  
   var glob_autocomplete;
   var glob_component_form = 
   {
     street_number: 'short_name',
     route: 'long_name',
     locality: 'long_name',
     administrative_area_level_1: 'long_name',
     postal_code: 'short_name'
   };
   
   var glob_options = {};
   glob_options.types = ['address'];
   
    $(document).ready(function(){
   
      $('#btn_add').click(function(){
      
      if($('#validation-form-user').parsley().validate() == false) return;

      $.ajax({
        url: '{{url('/admin/users/save')}}',
        type:"POST",
        data: new FormData($('#validation-form-user')[0]),
        contentType:false,
        processData:false,
        dataType:'json',
        success:function(data)
        {
         if('success' == data.status)
          {
            $('#validation-form-user')[0].reset();

            swal({
                title: data.status,
                text: data.description,
                type: data.status,
                confirmButtonText: "OK",
                closeOnConfirm: false
              },
              function(isConfirm,tmp)
              {
                if(isConfirm==true)
                {
                  window.location = data.link;
                }
              });
          }
          else
          {
             swal(data.status,data.description,data.status);
          }
        }
      });
     
     });
   });

   function changeCountryRestriction(ref)
   {
     var country_code = $(ref).val();
     destroyPlaceChangeListener(autocomplete);
     // load states function
     // loadStates(country_code);  
   
     glob_options.componentRestrictions = {country: country_code}; 
   
     initAutocomplete(country_code);
   
     glob_autocomplete = false;
     glob_autocomplete = initGoogleAutoComponent($('#autocomplete')[0],glob_options,glob_autocomplete);
   }
   
   
   function initAutocomplete(country_code) 
   {
     glob_options.componentRestrictions = {country: country_code}; 
   
     glob_autocomplete = false;
     glob_autocomplete = initGoogleAutoComponent($('#autocomplete')[0],glob_options,glob_autocomplete);
   }
   
   
   function initGoogleAutoComponent(elem,options,autocomplete_ref)
   {
     autocomplete_ref = new google.maps.places.Autocomplete(elem,options);
     autocomplete_ref = createPlaceChangeListener(autocomplete_ref,fillInAddress);
   
     return autocomplete_ref;
   }
   
   
   function createPlaceChangeListener(autocomplete_ref,fillInAddress)
   {
     autocomplete_ref.addListener('place_changed', fillInAddress);
     return autocomplete_ref;
   }
   
   function destroyPlaceChangeListener(autocomplete_ref)
   {
     google.maps.event.clearInstanceListeners(autocomplete_ref);
   }
   
   function fillInAddress() 
   {
     // Get the place details from the autocomplete object.
     var place = glob_autocomplete.getPlace();
     console.log(place)  ;
     for (var component in glob_component_form) 
     {
         $("#"+component).val("");
         $("#"+component).attr('disabled',false);
     }
     
     if(place.address_components.length > 0 )
     {
       $.each(place.address_components,function(index,elem)
       {
           var addressType = elem.types[0];
           if(glob_component_form[addressType])
           {
             var val = elem[glob_component_form[addressType]];
             $("#"+addressType).val(val) ;  
           }
       });  
     }
   }

  function getStates()
  {
     var site_url = "{{ url('/') }}";

     var country_id = $('#country').val();

      $.ajax({
                url       : site_url+'/admin/users/get_states/'+country_id,
                type      : "GET",
                 dataType  : "JSON",
                success:function(data)
                {
                  // console.log(data);
                  var html = "";
                  $(data).each(function(index,val){ 
                     html+="<option value='"+val['id']+"'>"+val['name']+"</option>";
                  });

                  $("#state").html(html);
                }
            });
      
  }  

  
   
</script>     
@stop
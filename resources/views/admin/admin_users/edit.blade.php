@extends('admin.layout.master')                
@section('main_content')
<!-- Page Content -->
<div id="page-wrapper">
<div class="container-fluid">
<div class="row bg-title">
   <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title">{{$page_title or ''}}</h4>
   </div>
   <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
      <ol class="breadcrumb">
         <li><a href="{{url('/')}}/admin/dashboard">Dashboard</a></li>
         <li><a href="{{$module_url_path}}">{{$module_title or ''}}</a></li>
         <li class="active">Edit Category</li>
      </ol>
   </div>
   <!-- /.col-lg-12 -->
</div>
<!-- BEGIN Main Content -->
<div class="row">
   <div class="col-md-12">
      <div class="white-box">
         @include('admin.layout._operation_status')  
         <div class="row">
            <div class="col-sm-12 col-xs-12">
               {!! Form::open(['method'=>'POST',
               'enctype' =>'multipart/form-data',   
               'class'=>'form-horizontal', 
               'id'=>'validation-form-admin-user' 
               ]) !!} 
               {{ csrf_field() }}
               <div class="form-group row">
                  <label class="col-2 col-form-label">Roles<i class="red">*</i></label>
                  <div class="col-10">
                     <?php 
                        $readonly= "";
                        ?>
                     @if(Sentinel::check()->inRole('admin')==true && Sentinel::check()->id==$obj_user->id)
                     <?php 
                        $readonly = "disabled";
                        ?>
                     @endif
                     <select class="form-control" name="roles[]" data-parsley-required {{ $readonly }}>
                     <option value="">-</option>
                     @if(isset($arr_roles) && sizeof($arr_roles)>0)
                     @foreach($arr_roles as $role)
                     @if($role['slug']=='admin')
                     <option value="{{ $role['id'] }}" @if(isset($arr_assigned_roles[0]) && $arr_assigned_roles[0]==$role['id']){{'selected'}}@endif>{{ $role['name'] }}</option>
                     @endif
                     @endforeach
                     @endif
                     </select>
                     <span class="red">{{ $errors->first('roles') }}</span>
                  </div>
               </div>
               <div class="form-group row">
                  <label class="col-2 col-form-label">First Name<i class="red">*</i></label>
                  <div class="col-10">
                     <input type="text" class="form-control" name="first_name" value="{{ $obj_user->first_name }}" data-parsley-required data-rule-maxlength="255" placeholder="Enter first name" />
                     <span class="red">{{ $errors->first('first_name') }}</span>
                  </div>
               </div>
               <div class="form-group row">
                  <label class="col-2 col-form-label">Last Name<i class="red">*</i></label>
                  <div class="col-10">
                     <input type="text" class="form-control" name="last_name" value="{{ $obj_user->last_name  }}" data-parsley-required data-rule-maxlength="255" placeholder="Enter last name"/>
                     <span class="red">{{ $errors->first('last_name') }}</span>
                  </div>
               </div>
               <div class="form-group row">
                  <label class="col-2 col-form-label">Email<i class="red">*</i></label>
                  <div class="col-10">
                     <input type="text" class="form-control" name="email" value="{{ $obj_user->email  }}" data-parsley-required  data-parsley-type="email" placeholder="Enter email"/>
                     <span class="red">{{ $errors->first('email') }}</span>
                  </div>
               </div>
               
               <div class="form-group row">
                  <label class="col-2 col-form-label">Password</label>
                  <div class="col-10">
                     <input type="password" class="form-control" name="password" value=""   id="password" placeholder="Enter password"/>
                     <span class="red">{{ $errors->first('password') }}</span>
                  </div>
               </div>
            
               <div class="form-group row">
                  <label class="col-2 col-form-label">Confirm Password</label>
                  <div class="col-10">
                     <input type="password" class="form-control" name="password_confirmation" data-parsley-equalto="#password"   placeholder="Enter Confirm password"/>
                     <span class="red">{{ $errors->first('password_confirmation') }}</span>
                  </div>
               </div>
               <div class="form-group row">
                  <div class="col-10">
                     <input type="hidden" name="enc_id" value="{{ $enc_id}}">
                     {{-- <button type="submit" class="btn btn-success waves-effect waves-light m-r-10" value="Update">Update</button> --}}

                     <button type="button" class="btn btn-success waves-effect waves-light m-r-10" id="btn_update" value="Update">Update</button>

                        <a class="btn btn-inverse waves-effect waves-light" href="{{$module_url_path}}">Back</a>
                  </div>
               </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- END Main Content -->
<script type="text/javascript">
   $(document).ready(function(){
    
      $('#btn_update').click(function(){

        // if($('#validation-form-admin-user').parsley().validate() == false) return;
         
         var formdata = $('#validation-form-admin-user').serialize();

         $.post('{{url('/admin/admin_users/save')}}',formdata,function(data,status){
            console.log(data.status);

            swal(data.status,data.description,data.status);
         });
      });
   });
</script>
@stop
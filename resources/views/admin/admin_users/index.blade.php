@extends('admin.layout.master')    
@section('main_content')
<link rel="stylesheet" type="text/css" href="{{url('/')}}/assets/data-tables/latest/dataTables.bootstrap.min.css">
<!-- Page Content -->

<div id="page-wrapper">
<div class="container-fluid">
<div class="row bg-title">
   <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title">{{$module_title or ''}}</h4>
   </div>
   <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
      <ol class="breadcrumb">
         <li><a href="{{url('/')}}/admin/dashboard">Dashboard</a></li>
         <li class="active">{{$module_title or ''}}</li>
      </ol>
   </div>
   <!-- /.col-lg-12 -->
</div>
<!-- BEGIN Main Content -->
<div class="row">
   <div class="col-md-12">
      <div class="white-box">
         {{-- @include('admin.layout._operation_status') --}}
       <div class="white-box">
         {{-- @include('admin.layout._operation_status') --}}
          {!! Form::open([ 'url' => $module_url_path.'/multi_action',
                                 'method'=>'GET',
                                 'enctype' =>'multipart/form-data',   
                                 'class'=>'form-horizontal', 
                                 'id'=>'frm_manage' 
                                ]) !!} 

            {{ csrf_field() }}

          <div class="pull-right">
            <a href="{{ url($module_url_path.'/create') }}" class="btn btn-outline btn-info btn-circle show-tooltip" title="Add More"><i class="fa fa-plus"></i> </a> 
              
            <a  href="javascript:void(0);" onclick="javascript : return check_multi_action('checked_record[]','frm_manage','activate');" class="btn btn-circle btn-success btn-outline show-tooltip" title="Multiple Unlock"><i class="ti-unlock"></i></a> 
            
            <a  href="javascript:void(0);" onclick="javascript : return check_multi_action('checked_record[]','frm_manage','deactivate');" class="btn btn-circle btn-danger btn-outline show-tooltip" title="Multiple Lock"><i class="ti-lock"></i> </a> 

            <a  href="javascript:void(0);" onclick="javascript : return check_multi_action('checked_record[]','frm_manage','delete');" class="btn btn-circle btn-danger btn-outline show-tooltip" title="Multiple Delete"><i class="ti-trash"></i> </a> 

            <a href="javascript:void(0)" onclick="javascript:location.reload();" class="btn btn-outline btn-info btn-circle show-tooltip" title="Refresh"><i class="fa fa-refresh"></i> </a> 
            
          </div>
        <br><br>
         <div class="table-responsive">
            <input type="hidden" name="multi_action" value="" />
            <table class="table table-striped"  id="table1" >
               <thead>
                  <tr>
                    <th>
                        <div class="checkbox checkbox-success">
                          <input class="checkboxInputAll" id="checkbox0" type="checkbox">
                          <label for="checkbox0">  </label>
                        </div>
                     </th>
                     <th>Id</th>
                     <th>First Name</th>
                     <th>Last Name</th>
                     <th>Email</th>
                     <th>Roles</th>
                     <th>Status</th>
                     <th>Action</th>
                  </tr>
               </thead>
               <tbody>
                  @if(sizeof($obj_users)>0)
                  @foreach($obj_users as $user)
                  <tr>
                   <td>
                      <div class="checkbox checkbox-success"><input type="checkbox" name="checked_record[]" value="{{$user->id}}" id="checkbox'{{$user->id}}'" class="case checkboxInput"/><label for="checkbox'{{$user->id}}"> </label></div>
                       
                      </div>
                    </td>
                     <td >
                        {{ $user->id or ''}}
                     </td>
                     <td >
                        {{ $user->first_name or 'N/A'}}
                     </td>
                     <td >
                        {{ $user->last_name or 'N/A'}}
                     </td>
                     <td >
                        {{ $user->email or 'N/A'}}
                     </td>
                     <td >
                        <?php 
                           $arr_roles = $user->roles->toArray();
                           $arr_roles = array_column($arr_roles,'name');
                           
                           echo implode(", ",$arr_roles);
                           
                           ?>
                     </td>
                     <td>
                        @if(isset($user->is_active) && $user->is_active=='1')
                           <input type="checkbox" checked data-size="small"  data-enc_id="'{{base64_encode($user->id)}}'"  id="status_'{{$user->id}}'" class="js-switch toggleSwitch" data-type="deactivate" data-color="#99d683" data-secondary-color="#f96262" />
                        @else
                           <input type="checkbox" data-size="small" data-enc_id="'{{base64_encode($user->id)}}'"  class="js-switch toggleSwitch" data-type="activate" data-color="#99d683" data-secondary-color="#f96262" />
                        @endif
                     </td>
                     <td> 
                        <a class="btn btn-outline btn-info btn-circle show-tooltip" href="{{ url($module_url_path.'/edit')."/".base64_encode($user->id) }}" title="Edit"><i class="ti-pencil-alt2" ></i></a>
                        @if($is_last_user==false)
                        <a class="btn btn-circle btn-danger btn-outline show-tooltip" href="{{ url($module_url_path.'/delete')."/".base64_encode($user->id) }}" onclick="confirm_action(this,event,'Are you sure to delete this record?')" title="Delete"><i class="ti-trash" ></i></a>
                        @endif
                     </td>
                  </tr>
                  @endforeach
                  @endif
               </tbody>
            </table>
         </div>
         <div> </div>
         </form>
      </div>
   </div>
</div>
<!-- END Main Content -->
<script type="text/javascript">
   var module_url_path         = "{{ $module_url_path }}";
   
  $(document).ready(function(){
    $('#table1').DataTable({});

  });

   function show_details(url)
   { 
      
       window.location.href = url;
   } 
   
   function confirm_delete()
   {
      if(confirm('Are you sure ?'))
      {
       return true;
      }
      return false;
   }

   function statusChange(data)
   {
   var ref = data; 
   var type = data.attr('data-type');
   var enc_id = data.attr('data-enc_id');
   var id = data.attr('data-id');
  
     $.ajax({
       url:module_url_path+'/'+type,
       type:'GET',
       data:{id:enc_id},
       dataType:'json',
       success: function(response)
       {
         if(response.status=='SUCCESS'){
           if(response.data=='ACTIVE')
           {
             $(ref)[0].checked = true;  
             $(ref).attr('data-type','deactivate');
   
           }else
           {
             $(ref)[0].checked = false;  
             $(ref).attr('data-type','activate');
           }
         }
         else
         {
           sweetAlert('Error','Something went wrong!','error');
         }  
       }
     });  
   } 
    $(function(){

   $("input.checkboxInputAll").click(function(){

      if($("input.checkboxInput:checkbox:checked").length <= 0){
          $("input.checkboxInput").prop('checked',true);
      }else{
          $("input.checkboxInput").prop('checked',false);
      }

   }); 
});
   
</script>
@stop
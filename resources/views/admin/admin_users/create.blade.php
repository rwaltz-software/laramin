@extends('admin.layout.master')
@section('main_content')
<!-- Page Content -->
<div id="page-wrapper">
<div class="container-fluid">
<div class="row bg-title">
   <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title">{{$page_title or ''}}</h4>
   </div>
   <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
      <ol class="breadcrumb">
         <li><a href="{{url('/')}}/admin/dashboard">Dashboard</a></li>
         <li><a href="{{$module_url_path}}">{{$module_title or ''}}</a></li>
         <li class="active">Add {{$module_title or ''}}</li>
      </ol>
   </div>
   <!-- /.col-lg-12 -->
</div>
<!-- BEGIN Main Content -->
<div class="row">
   <div class="col-md-12">
      <div class="white-box">
         @include('admin.layout._operation_status')  
         <div class="row">
            <div class="col-sm-12 col-xs-12">
               {!! Form::open([ 'url' => $module_url_path.'/save',
               'method'=>'POST',
               'enctype' =>'multipart/form-data',   
               'class'=>'form-horizontal', 
               'id'=>'validation-form',
               'data-parsley-validate'=>"" 
               ]) !!} 
               {{ csrf_field() }}
               <div class="form-group row">
                  <label class="col-2 col-form-label">Roles<i class="red">*</i></label>
                  <div class="col-10">
                     <select class="form-control" name="roles[]" required="true">
                        <option value="">-</option>
                        @if(isset($arr_roles) && sizeof($arr_roles)>0)
                        @foreach($arr_roles as $role)
                        @if($role['slug']=='admin')
                        <option value="{{ $role['id'] }}">{{ $role['name'] }}</option>
                        @endif
                        @endforeach
                        @endif
                     </select>
                     <span class="help-block">{{ $errors->first('roles') }}</span>
                  </div>
               </div>
               <div class="form-group row">
                  <label class="col-2 col-form-label">First Name<i class="red">*</i></label>
                  <div class="col-10">
                     <input type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" required="true"  placeholder="Enter first name" />
                     <span class="red">{{ $errors->first('first_name') }}</span>
                  </div>
               </div>
               <div class="form-group row">
                  <label class="col-2 col-form-label">Last Name<i class="red">*</i></label>
                  <div class="col-10">
                     <input type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" required="true"  placeholder="Enter last name" />
                     <span class="red">{{ $errors->first('last_name') }}</span>
                  </div>
               </div>
               <div class="form-group row">
                  <label class="col-2 col-form-label">Email<i class="red">*</i></label>
                  <div class="col-10">
                     <input type="text" class="form-control" name="email" value="{{ old('email') }}" required data-parsley-type="email" placeholder="Enter Email" />
                     <span class="red">{{ $errors->first('email') }}</span>
                  </div>
               </div>
               <div class="form-group row">
                  <label class="col-2 col-form-label">Password<i class="red">*</i></label>
                  <div class="col-10">
                     <input type="password" class="form-control" name="password" value="{{ old('password') }}" required="true" data-parsley-min="6" id="password" placeholder="Enter Password"/>
                     <span class="red">{{ $errors->first('password') }}</span>
                  </div>
               </div>
               <div class="form-group row">
                  <label class="col-2 col-form-label">Confirm Password<i class="red">*</i></label>
                  <div class="col-10">
                     <input type="password" class="form-control" name="password_confirmation" value="{{ old('password') }}" required="true" data-parsley-equalto="#password" placeholder="Enter Confirm password"/>
                     <span class="red">{{ $errors->first('password_confirmation') }}</span>
                  </div>
               </div>
               
               <div class="form-group row">
                  <div class="col-10">
                    {{--  <button type="submit" class="btn btn-success waves-effect waves-light m-r-10" value="Save">Save</button> --}}
                     <button type="button" class="btn btn-success waves-effect waves-light m-r-10" id="btn_add" value="Save">Save</button>

                     <a class="btn btn-inverse waves-effect waves-light" href="{{$module_url_path}}">Back</a>

                  </div>
               </div>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- END Main Content -->
<script type="text/javascript">
   $(document).ready(function(){

   $('#btn_add').click(function(){

      if($('#validation-form').parsley().validate() == false) return;

      var formdata = $('#validation-form').serialize();
      $.post('{{url('/admin/admin_users/save')}}',formdata,function(data,status){
        

        if('success' == data.status)
         {
           $('#validation-form')[0].reset();

           swal({
               title: data.status,
               text: data.description,
               type: data.status,
               confirmButtonText: "OK",
               closeOnConfirm: false
             },
             function(isConfirm,tmp)
             {
               if(isConfirm==true)
               {
                 window.location = data.link;
               }
             });
         }
         else
         {
            swal(data.status,data.description,data.status);
         }
      });
      // .then(function(data)
      // { 
             // console.log(data);
        
      // }).catch(function(error)
      // {
      //   console.log(error);
      // });
  });

});
</script>
@stop
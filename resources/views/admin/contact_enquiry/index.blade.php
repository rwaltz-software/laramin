@extends('admin.layout.master')                
@section('main_content')
<!-- BEGIN Page Title -->
<link rel="stylesheet" type="text/css" href="{{url('/')}}/assets/data-tables/latest/dataTables.bootstrap.min.css">
<!-- Page Content -->
<div id="page-wrapper">
<div class="container-fluid">
   <div class="row bg-title">
      <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
         <h4 class="page-title">{{$module_title or ''}}</h4>
      </div>
      <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
         <ol class="breadcrumb">
            <li><a href="{{url('/')}}/admin/dashboard">Dashboard</a></li>
            <li class="active">{{$module_title or ''}}</li>
         </ol>
      </div>
   </div>
   <!-- BEGIN Main Content -->
   <div class="col-sm-12">
      <div class="white-box">
         {{-- @include('admin.layout._operation_status') --}}
          {!! Form::open([ 'url' => $module_url_path.'/multi_action',
                                 'method'=>'POST',
                                 'enctype' =>'multipart/form-data',   
                                 'class'=>'form-horizontal', 
                                 'id'=>'frm_manage' 
                                ]) !!} 

            {{ csrf_field() }}

         
          <div class="pull-right">
                <a  href="javascript:void(0);" onclick="javascript : return check_multi_action('checked_record[]','frm_manage','delete');" class="btn btn-circle btn-danger btn-outline show-tooltip" title="Multiple Delete"><i class="ti-trash"></i> </a> 

                <a href="javascript:void(0)" onclick="javascript:location.reload();" class="btn btn-outline btn-info btn-circle show-tooltip" title="Refresh"><i class="fa fa-refresh"></i> </a> 
          </div>
          <br/>
          <br>
          <div class="table-responsive">

            <input type="hidden" name="multi_action" value="" />
            <table class="table table-striped"  id="table_module" >
              <thead>
                <tr>
                  <th> 
                  <div class="checkbox checkbox-success">
                      <input class="checkboxInputAll" id="checkbox0" type="checkbox">
                      <label for="checkbox0">  </label>
                  </div>
                  </th>
                  <th>User Name</th> 
                  <th>Email</th> 
                  <th>Phone</th> 
                  <th>Subject</th> 
                  <th>Read/Unread</th>
                  <th>Action</th>
                </tr>
              </thead>
              <tbody>
        
                @if(sizeof($arr_contact_enquiry)>0)
                  @foreach($arr_contact_enquiry as $contact_enquiry)
              
                  <tr>
                    <td> 
                    <div class="checkbox checkbox-success">
                    <input type="checkbox" name="checked_record[]" value="{{ base64_encode($contact_enquiry['id']) }}" id="checkbox{{$contact_enquiry['id']}}" class="case checkboxInput"/><label for="checkbox{{$contact_enquiry['id']}}">  </label></div>
                    </td>
                    <td > {{ $contact_enquiry['user_name'] }} </td> 
                    <td > {{ $contact_enquiry['email'] }} </td>   
                    <td > {{ $contact_enquiry['phone'] }} </td> 
                    <td > {{ $contact_enquiry['subject'] }} </td> 
                    <td>
                      @if($contact_enquiry['is_view']==1)
                          <a href="javascript:void(0)" class="show-tooltip" title="Viewed Contact Enquiry">
                            <i class="fa fa-check" style="color:green;"></i>
                          </a>  
                        @elseif($contact_enquiry['is_view']==0)
                          <a href="javascript:void(0)" class="show-tooltip" title="Not-Viewed Contact Enquiry">
                            <i class="fa fa-times" style="color:red;"></i>
                          </a>  
                        @endif
                    </td>
                    <td> 
                      <a class="btn btn-circle btn-success btn-outline show-tooltip" href="{{ $module_url_path.'/view/'.base64_encode($contact_enquiry['id']) }}"><i class="ti-eye"  title="View"></i>
                      </a>  
                      <a class="btn btn-circle btn-danger btn-outline show-tooltip" href="{{ $module_url_path.'/delete/'.base64_encode($contact_enquiry['id']) }}" onclick="return confirm_action(this,event,'Are you sure to delete this record?');" title="Delete">
                        <i class="ti-trash" ></i>  
                      </a>  
                    </td>
                  </tr>
                  @endforeach
                @endif
                 
              </tbody>
            </table>
          </div>
        <div> </div>
         
          {!! Form::close() !!}
      </div>
  </div>
</div>

<!-- END Main Content -->
<script type="text/javascript">
     $(document).ready(function() {
       $('#table_module').DataTable( {
            "aoColumns": [
            { "bSortable": false },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": true },
            { "bSortable": false }
            ]

        });
    });

   $(function(){
       $("input.checkboxInputAll").click(function(){
          if($("input.checkboxInput:checkbox:checked").length <= 0){
              $("input.checkboxInput").prop('checked',true);
          }else{
              $("input.checkboxInput").prop('checked',false);
          }

       }); 
    });
</script>
@stop                    



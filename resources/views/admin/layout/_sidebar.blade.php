@php
    $admin_path     = config('app.project.admin_panel_slug');
    $obj_data  = Sentinel::getUser();
    
    if($obj_data)
    {
       $arr_data = $obj_data->toArray();  
    }               
@endphp

 <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header"> <a class="navbar-toggle hidden-sm hidden-md hidden-lg " href="javascript:void(0)" data-toggle="collapse" data-target=".navbar-collapse"><i class="ti-menu"></i></a>
                <div class="top-left-part"><a class="logo" href="{{url('/')}}/admin/dashboard"><b><img src="{{url('/')}}/uploads/profile_image/{{$site_setting_arr['site_logo']}}" height="30"  width="40" alt="home" /></b>

                <span class="hidden-xs"> <img src="{{ url('/') }}/assets/images/laramin.png" width="120px" height="50px" alt="homepage" class="dark-logo" /></span></a></div>
                 {{-- <img src="{{ url('/') }}/assets/images/laramin.png" width="120px" height="50px" alt="homepage" class="dark-logo" /> --}}

                <ul class="nav navbar-top-links navbar-left hidden-xs">
                    <li><a href="javascript:void(0)" class="open-close hidden-xs waves-effect waves-light"><i class="icon-arrow-left-circle ti-menu"></i></a></li>
                    {{-- <li>
                        <form role="search" class="app-search hidden-xs">
                            <input type="text" placeholder="Search..." class="form-control"> <a href=""><i class="fa fa-search"></i></a> </form>
                    </li> --}}
                </ul>
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li class="dropdown">
                    {{--  <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="icon-envelope"></i>
          <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
          </a> --}}
                        <ul class="dropdown-menu mailbox animated bounceInDown">
                            <li>
                                <div class="drop-title">You have 4 new messages</div>
                            </li>
                            <li>
                                <div class="message-center">
                                    <a href="#">
                                        <div class="user-img"> <img src="{{url('/')}}/assets/plugins/images/users/pawandeep.jpg" alt="user" class="img-circle"> <span class="profile-status online pull-right"></span> </div>
                                        <div class="mail-contnet">
                                            <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:30 AM</span> </div>
                                    </a>
                                    <a href="#">
                                        <div class="user-img"> <img src="{{url('/')}}/assets/plugins/images/users/sonu.jpg" alt="user" class="img-circle"> <span class="profile-status busy pull-right"></span> </div>
                                        <div class="mail-contnet">
                                            <h5>Sonu Nigam</h5> <span class="mail-desc">I've sung a song! See you at</span> <span class="time">9:10 AM</span> </div>
                                    </a>
                                    <a href="#">
                                        <div class="user-img"> <img src="{{url('/')}}/assets/plugins/images/users/arijit.jpg" alt="user" class="img-circle"> <span class="profile-status away pull-right"></span> </div>
                                        <div class="mail-contnet">
                                            <h5>Arijit Sinh</h5> <span class="mail-desc">I am a singer!</span> <span class="time">9:08 AM</span> </div>
                                    </a>
                                    <a href="#">
                                        <div class="user-img"> <img src="{{url('/')}}/assets/plugins/images/users/pawandeep.jpg" alt="user" class="img-circle"> <span class="profile-status offline pull-right"></span> </div>
                                        <div class="mail-contnet">
                                            <h5>Pavan kumar</h5> <span class="mail-desc">Just see the my admin!</span> <span class="time">9:02 AM</span> </div>
                                    </a>
                                </div>
                            </li>
                            <li>
                                <a class="text-center" href="javascript:void(0);"> <strong>See all notifications</strong> <i class="fa fa-angle-right"></i> </a>
                            </li>
                        </ul>
                        <!-- /.dropdown-messages -->
                    </li>
                    <!-- /.dropdown -->
                    <li class="dropdown"> 
                    {{-- <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><i class="icon-note"></i>
          <div class="notify"><span class="heartbit"></span><span class="point"></span></div>
          </a>
                        <ul class="dropdown-menu dropdown-tasks animated slideInUp">
                            <li>
                                <a href="#">
                                    <div>
                                        <p> <strong>Task 1</strong> <span class="pull-right text-muted">40% Complete</span> </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100" style="width: 40%"> <span class="sr-only">40% Complete (success)</span> </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <p> <strong>Task 2</strong> <span class="pull-right text-muted">20% Complete</span> </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-info" role="progressbar" aria-valuenow="20" aria-valuemin="0" aria-valuemax="100" style="width: 20%"> <span class="sr-only">20% Complete</span> </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <p> <strong>Task 3</strong> <span class="pull-right text-muted">60% Complete</span> </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: 60%"> <span class="sr-only">60% Complete (warning)</span> </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="#">
                                    <div>
                                        <p> <strong>Task 4</strong> <span class="pull-right text-muted">80% Complete</span> </p>
                                        <div class="progress progress-striped active">
                                            <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 80%"> <span class="sr-only">80% Complete (danger)</span> </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a class="text-center" href="#"> <strong>See All Tasks</strong> <i class="fa fa-angle-right"></i> </a>
                            </li>
                        </ul>
                        <!-- /.dropdown-tasks --> --}}
                    </li>
                    

                    <!-- /.dropdown -->
                    <li class="dropdown">
                        <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img src="{{url('/')}}/uploads/profile_image/{{$arr_data['profile_image']}}" alt="user-img" width="36" class="img-circle"><b class="hidden-xs">{{$arr_data['first_name'] or ''}}</b> </a>
                        <ul class="dropdown-menu dropdown-user animated flipInY">
                        <li><a href="{{url('/')}}"><i class="ti-world"></i> Visit Website</a></li>
                        <li><a href="{{ url('/').'/'.$admin_path.'/change_password' }}"><i class="ti-key"></i> Change Password</a></li>
                        <li role="separator" class="divider"></li>
                            
                        <li><a href="{{ url('/').'/'.$admin_path }}/logout"><i class="fa fa-power-off"></i> Logout</a></li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>
                    <!-- .Megamenu -->
                    <li class="mega-dropdown"> 
                    {{-- <a class="dropdown-toggle waves-effect waves-light" data-toggle="dropdown" href="#"><span class="hidden-xs">Mega</span> <i class="icon-options-vertical"></i></a> --}}
                        <ul class="dropdown-menu mega-dropdown-menu animated bounceInDown">
                            <li class="col-sm-3">
                                <ul>
                                    <li class="dropdown-header">Forms Elements</li>
                                    <li><a href="form-basic.html">Basic Forms</a></li>
                                    <li><a href="form-layout.html">Form Layout</a></li>
                                    <li><a href="form-advanced.html">Form Addons</a></li>
                                    <li><a href="form-material-elements.html">Form Material</a></li>
                                    <li><a href="form-float-input.html">Form Float Input</a></li>
                                    <li><a href="form-upload.html">File Upload</a></li>
                                    <li><a href="form-mask.html">Form Mask</a></li>
                                    <li><a href="form-img-cropper.html">Image Cropping</a></li>
                                    <li><a href="form-validation.html">Form Validation</a></li>
                                </ul>
                            </li>
                            <li class="col-sm-3">
                                <ul>
                                    <li class="dropdown-header">Advance Forms</li>
                                    <li><a href="form-dropzone.html">File Dropzone</a></li>
                                    <li><a href="form-pickers.html">Form-pickers</a></li>
                                    <li><a href="icheck-control.html">Icheck Form Controls</a></li>
                                    <li><a href="form-wizard.html">Form-wizards</a></li>
                                    <li><a href="form-typehead.html">Typehead</a></li>
                                    <li><a href="form-xeditable.html">X-editable</a></li>
                                    <li><a href="form-summernote.html">Summernote</a></li>
                                    <li><a href="form-bootstrap-wysihtml5.html">Bootstrap wysihtml5</a></li>
                                    <li><a href="form-tinymce-wysihtml5.html">Tinymce wysihtml5</a></li>
                                </ul>
                            </li>
                            <li class="col-sm-3">
                                <ul>
                                    <li class="dropdown-header">Table Example</li>
                                    <li><a href="basic-table.html">Basic Tables</a></li>
                                    <li><a href="table-layouts.html">Table Layouts</a></li>
                                    <li><a href="data-table.html">Data Table</a></li>
                                    <li class="hidden"><a href="crud-table.html">Crud Table</a></li>
                                    <li><a href="bootstrap-tables.html">Bootstrap Tables</a></li>
                                    <li><a href="responsive-tables.html">Responsive Tables</a></li>
                                    <li><a href="editable-tables.html">Editable Tables</a></li>
                                    <li><a href="foo-tables.html">FooTables</a></li>
                                    <li><a href="jsgrid.html">JsGrid Tables</a></li>
                                </ul>
                            </li>
                            <li class="col-sm-3">
                                <ul>
                                    <li class="dropdown-header">Charts</li>
                                    <li> <a href="flot.html">Flot Charts</a> </li>
                                    <li><a href="morris-chart.html">Morris Chart</a></li>
                                    <li><a href="chart-js.html">Chart-js</a></li>
                                    <li><a href="peity-chart.html">Peity Charts</a></li>
                                    <li><a href="knob-chart.html">Knob Charts</a></li>
                                    <li><a href="sparkline-chart.html">Sparkline charts</a></li>
                                    <li><a href="extra-charts.html">Extra Charts</a></li>
                                </ul>
                            </li>
                            <li class="col-sm-12 m-t-40 demo-box">
                                <div class="row">
                                    <div class="col-sm-2">
                                        <div class="white-box text-center bg-purple"><a href="http://eliteadmin.themedesigner.in/demos/eliteadmin-inverse/index.html" target="_blank" class="text-white"><i class="linea-icon linea-basic fa-fw" data-icon="v"></i><br/>Demo 1</a></div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="white-box text-center bg-success"><a href="http://eliteadmin.themedesigner.in/demos/eliteadmin/index.html" target="_blank" class="text-white"><i class="linea-icon linea-basic fa-fw" data-icon="v"></i><br/>Demo 2</a></div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="white-box text-center bg-info"><a href="http://eliteadmin.themedesigner.in/demos/eliteadmin-ecommerce/index.html" target="_blank" class="text-white"><i class="linea-icon linea-basic fa-fw" data-icon="v"></i><br/>Demo 3</a></div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="white-box text-center bg-inverse"><a href="http://eliteadmin.themedesigner.in/demos/eliteadmin-horizontal-navbar/index3.html" target="_blank" class="text-white"><i class="linea-icon linea-basic fa-fw" data-icon="v"></i><br/>Demo 4</a></div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="white-box text-center bg-warning"><a href="http://eliteadmin.themedesigner.in/demos/eliteadmin-iconbar/index4.html" target="_blank" class="text-white"><i class="linea-icon linea-basic fa-fw" data-icon="v"></i><br/>Demo 5</a></div>
                                    </div>
                                    <div class="col-sm-2">
                                        <div class="white-box text-center bg-danger"><a href="https://themeforest.net/item/elite-admin-responsive-web-app-kit-/16750820?ref=suniljoshi" target="_blank" class="text-white"><i class="linea-icon linea-ecommerce fa-fw" data-icon="d"></i><br/>Buy Now</a></div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- /.Megamenu -->
                   {{--  <li class="right-side-toggle"> <a class="waves-effect waves-light" href="javascript:void(0)"><i class="ti-settings"></i></a></li> --}}
                    <!-- /.dropdown -->
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
        <!-- Left navbar-header -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav navbar-collapse slimscrollsidebar">
                <ul class="nav" id="side-menu">
                    <li class="sidebar-search hidden-sm hidden-md hidden-lg">
                        <!-- input-group -->
                        <div class="input-group custom-search-form">
                            <input type="text" class="form-control" placeholder="Search..."> <span class="input-group-btn">
            <button class="btn btn-default" type="button"> <i class="fa fa-search"></i> </button>
            </span> </div>
                        <!-- /input-group -->
                    </li>
                    <li class="user-pro">
                        <a href="#" class="waves-effect"><img src="{{url('/')}}/uploads/profile_image/{{$arr_data['profile_image']}}" class="img-circle"> <span class="hide-menu"> {{$arr_data['first_name'] or ''}}<span class="fa arrow"></span></span>
                        </a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{ url('/').'/'.$admin_path.'/account_settings' }}"><i class="ti-settings"></i> Account Setting</a></li>
                            <li><a href="{{ url('/').'/'.$admin_path }}/logout"><i class="fa fa-power-off"></i> Logout</a></li>
                        </ul>
                    </li>

                    <li class="<?php  if(Request::segment(2) == 'dashboard'){ echo 'active'; } ?>"> <a href="{{ url('/').'/'.$admin_path.'/dashboard'}}" class="waves-effect"><i data-icon="P" class=" ti-dashboard"></i> <span class="hide-menu"> Dashboard</span></a> </li>
                    <li class="<?php  if(Request::segment(2) == 'product'){ echo 'active'; } ?>"> <a href="{{ url('/').'/'.$admin_path.'/product'}}" class="waves-effect"><i data-icon="P" class=" ti-crown"></i> <span class="hide-menu"> Product</span></a> </li>

                <!--    <li class="<?php  if(Request::segment(2) == 'categories'){ echo 'active'; } ?>"> <a href="{{ url('/').'/'.$admin_path.'/dashboard'}}" class="waves-effect"><i data-icon="P" class=" ti-dashboard"></i> <span class="hide-menu"> Dashboard</span></a> </li>

                    <li class="<?php  if(Request::segment(2) == 'activity_logs'){ echo 'active'; } ?>">
                    <a href="{{ url('/').'/'.$admin_path.'/activity_logs' }}" class="waves-effect"><i data-icon="P" class="icon-bubble"></i> <span class="hide-menu">Activity Log</span></a>
                    </li>

                    <li class="<?php  if(Request::segment(2) == 'categories'){ echo 'active'; } ?>"> <a href="{{ url('/').'/'.$admin_path.'/categories'}}" class="waves-effect"><i data-icon="P" class=" ti-crown"></i> <span class="hide-menu"> Categories</span></a> </li>

                    <li class="<?php  if(Request::segment(2) == 'site_settings'){ echo 'active'; } ?>"> <a href="{{ url('/')}}/admin/site_settings" class="waves-effect"><i data-icon="P" class="linea-icon linea-basic fa-fw"></i> <span class="hide-menu"> Site Settings</span></a> </li>

                    <li class="<?php  if(Request::segment(2) == 'admin_users'){ echo 'active'; } ?>"> <a href="{{ url($admin_panel_slug.'/admin_users')}}" class="waves-effect"><i data-icon="P" class="ti-user"></i> <span class="hide-menu"> Admin Users</span></a> </li>

                    <li class="<?php  if(Request::segment(2) == 'users'){ echo 'active'; } ?>"> <a href="{{ url($admin_panel_slug.'/users')}}" class="waves-effect"><i data-icon="P" class="icon-people"></i> <span class="hide-menu"> Users</span></a> </li>

                    <li class="<?php  if(Request::segment(2) == 'email_template'){ echo 'active'; } ?>"> <a href="{{ url($admin_panel_slug.'/email_template')}}" class="waves-effect"><i data-icon="P" class="fa-fw ti-email"></i> <span class="hide-menu">Email Templates</span></a> </li>

                    <li class="<?php  if(Request::segment(2) == 'static_pages'){ echo 'active'; } ?>"> <a href="{{ url($admin_panel_slug.'/static_pages')}}" class="waves-effect"><i data-icon="P" class="ti-layers-alt"></i> <span class="hide-menu">CMS</span></a> </li>
                    <li class="<?php  if(Request::segment(2) == 'faq'){ echo 'active'; } ?>"> <a href="{{ url($admin_panel_slug.'/faq')}}" class="waves-effect"><i data-icon="P" class="ti-help-alt"></i> <span class="hide-menu"> FAQ</span></a> </li>
                    <li class="<?php  if(Request::segment(2) == 'faq'){ echo 'active'; } ?>"> <a href="{{ url($admin_panel_slug.'/contact_enquiry')}}" class="waves-effect"><i data-icon="P" class="icon-phone"></i> <span class="hide-menu"> Contact Enquires</span></a> </li>

                    <li class="user-pro">
                        <a href="#" class="waves-effect"><i data-icon="P" class="ti-location-pin"></i><span class="hide-menu">Locations<span class="fa arrow"></span></span></a>
                    
                        <ul class="nav nav-second-level">
                            <li><a href="{{ url('/').'/'.$admin_path.'/countries'}}"><i class="icon-flag"></i> Manage Country</a></li>
                            <li><a href="{{ url('/').'/'.$admin_path.'/states'}}"><i class=" ti-location-arrow"></i> Manage State/Regions</a></li>
                            <li><a href="{{ url('/').'/'.$admin_path.'/cities'}}"><i class=" ti-map-alt"></i> Manage Cities</a></li>
                        </ul>
                    </li>
                    <li class="<?php  if(Request::segment(2) == 'faq'){ echo 'active'; } ?>"> <a href="{{ url('/').'/'.$admin_path.'/keyword_translation' }}" class="waves-effect"><i data-icon="P" class="icon-bubble"></i> <span class="hide-menu">Keyword Translation</span></a> </li>
                     </li>-->
                    
                </ul>
            </div>
        </div>
        <!-- Left navbar-header end -->


@extends('admin.layout.master')                
@section('main_content')
<!-- Page Content -->
<div id="page-wrapper">
<div class="container-fluid">
   <div class="row bg-title">
      <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
         <h4 class="page-title">{{$page_title or ''}}</h4>
      </div>
      <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
         <ol class="breadcrumb">
            <li><a href="{{url('/')}}/admin/dashboard">Dashboard</a></li>
            <li><a href="{{$module_url_path}}">{{$module_title or ''}}</a></li>
            <li class="active">Edit Category</li>
         </ol>
      </div>
      <!-- /.col-lg-12 -->
   </div>
   <!-- .row -->
   <div class="row">
      <div class="col-sm-12">
         <div class="white-box">
            @include('admin.layout._operation_status')
            {!! Form::open([ 
                           'method'=>'POST',
                           'enctype' =>'multipart/form-data',   
                           'class'=>'form-horizontal', 
                           'id'=>'validation-form' 
            ]) !!} 
            <ul  class="nav nav-tabs">
               @include('admin.layout._multi_lang_tab')
            </ul>
            <div id="myTabContent1" class="tab-content">
               @if(isset($arr_lang) && sizeof($arr_lang)>0)
               
               @foreach($arr_lang as $lang)
               <?php 
                     /* Locale Variable */  
                     $locale_question = "";
                     $locale_answer   = "";
                     
                     if(isset($arr_data['translations'][$lang['locale']]))
                     {
                       $locale_question = $arr_data['translations'][$lang['locale']]['question'];
                        $locale_answer  = $arr_data['translations'][$lang['locale']]['answer'];
                     }
                     ?>
                 <div class="tab-pane fade {{ $lang['locale']=='en'?'in active':'' }}" 
                     id="{{ $lang['locale'] }}">

                     <div class="form-group row">
                        <label class="col-2 col-form-label" for="state"> Question
                        @if($lang['locale'] == 'en') 
                        <i class="red">*</i>
                        @endif
                        </label>
                        <div class="col-10">
                           @if($lang['locale'] == 'en')        
                           {!! Form::text('question_'.$lang['locale'],$locale_question,['class'=>'form-control','data-rule-required'=>'true','data-rule-maxlength'=>'500','id' => 'question', 'placeholder'=>'Question']) !!}
                           @else
                           {!! Form::text('question_'.$lang['locale'],$locale_question,['class'=>'form-control', 'placeholder'=>'Question']) !!}
                           @endif    
                        </div>
                        <span class='red'>{{ $errors->first('question_'.$lang['locale']) }}</span>  
                     </div>
                     <div class="form-group row">
                        <label class="col-2 col-form-label" for="state"> Answer
                        @if($lang['locale'] == 'en') 
                        <i class="red">*</i>
                        @endif
                        </label>
                        <div class="col-10">
                           @if($lang['locale'] == 'en')        
                           {!! Form::textarea('answer_'.$lang['locale'],$locale_answer,['class'=>'form-control answer','data-rule-required'=>'true','rows'=>'20', 'placeholder'=>'Answer','id'=>'answer_'.$lang['locale']]) !!}
                           @else
                           {!! Form::textarea('answer_'.$lang['locale'],$locale_answer,['class'=>'form-control answer', 'placeholder'=>'Answer','id'=>'answer_'.$lang['locale']]) !!}
                           @endif    
                        </div>
                        <span class='red'>{{ $errors->first('answer_'.$lang['locale']) }}</span>  
                     </div>
                  </div>

                  {{-- <input type="hidden" name="arrr" id="arrr" value="{{ $arr_lang[1]['locale'] }}"> --}}
               @endforeach
               
               @endif
            </div>

            <br>
            <div class="form-group row">
               <div class="col-10">
                  <input type="hidden" name="enc_id" value="{{$enc_id}}">
                  
                  {{-- <button type="submit" onclick="saveTinyMceContent();" class="btn btn-success waves-effect waves-light m-r-10" value="Update">Update</button> --}}

                   <button type="button" onclick="saveTinyMceContent();" class="btn btn-success waves-effect waves-light m-r-10" value="Update" id="btn_update" >Update</button>

                  <a class="btn btn-inverse waves-effect waves-light" href="{{$module_url_path}}">Back</a>
               </div>
            </div>
            {!! Form::close() !!}
         </div>
      </div>
   </div>
</div>

<!-- END Main Content -->
<script type="text/javascript">

  $(document).ready(function(){

   // CKEDITOR.replaceAll();

   $('#btn_update').click(function(){
      

    $( 'textarea.answer').each( function() {

     var txt_data = ($(this).val());
     
      });


      if($('#validation-form').parsley().validate() == false) return;

      var formdata = $('#validation-form').serialize();

      console.log(formdata);
      
      $.post('{{url('/admin/faq/save')}}',formdata,function(data,status){
        
         swal(data.status,data.description,data.status);
      });
  });

});

</script>
<script type="text/javascript" src="{{url('/assets/js/tinyMCE.js')}}"></script>
<script type="text/javascript">
   function saveTinyMceContent()
   {
     tinyMCE.triggerSave();
   }
</script>
@stop
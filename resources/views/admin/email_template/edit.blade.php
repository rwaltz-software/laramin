@extends('admin.layout.master')                
@section('main_content')
<!-- Page Content -->
<div id="page-wrapper">
<div class="container-fluid">
<div class="row bg-title">
   <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title">{{$page_title or ''}}</h4>
   </div>
   <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
      <ol class="breadcrumb">
         <li><a href="{{url('/')}}/admin/dashboard">Dashboard</a></li>
         <li><a href="{{$module_url_path}}">{{$module_title or ''}}</a></li>
         <li class="active">{{$page_title or ''}}</li>
      </ol>
   </div>
   <!-- /.col-lg-12 -->
</div>
<!-- .row -->
<div class="row">
<div class="col-sm-12">
   <div class="white-box">
      @include('admin.layout._operation_status')
      <div class="row">
         <div class="col-sm-12 col-xs-12">
            {!! Form::open([
                           'method'=>'POST',
                           'enctype' =>'multipart/form-data',   
                           'class'=>'form-horizontal', 
                           'id'=>'validation-form' 
            ]) !!} 
            {{ csrf_field() }}
            <div class="tab-content">
               <div class="form-group row">
                  <label class="col-2 col-form-label" for="email"> Template Name<i class="red">*</i> 
                  </label>
                  <div class="col-10">       
                     {!! Form::text('template_name',$arr_data['template_name'],['class'=>'form-control','data-parsley-required'=>'true','data-parsley-maxlength'=>'255', 'placeholder'=>'Email Template Name']) !!}  
                  </div>
                  <span class='red'> {{ $errors->first('template_name') }} </span>  
               </div>
               <div class="form-group row">
                  <label class="col-2 col-form-label" for="email"> From 
                  <i class="red">*</i> 
                  </label>
                  <div class="col-10">      
                     {!! Form::text('template_from',$arr_data['template_from'],['class'=>'form-control','data-parsley-required'=>'true','data-parsley-maxlength'=>'255', 'placeholder'=>'Email Template From']) !!}  
                  </div>
                  <span class='help-block'> {{ $errors->first('template_from') }} </span>  
               </div>
               <div class="form-group row">
                  <label class="col-2 col-form-label" for="email">  From Email 
                  <i class="red">*</i> 
                  </label>
                  <div class="col-10">     
                     {!! Form::text('template_from_mail',$arr_data['template_from_mail'],['class'=>'form-control','data-parsley-required'=>'true','data-parsley-maxlength'=>'255','data-parsley-type'=>'email', 'placeholder'=>'Email Template From Email']) !!}  
                  </div>
                  <span class='help-block'> {{ $errors->first('template_from_mail') }} </span>  
               </div>
               <div class="form-group row">
                  <label class="col-2 col-form-label" for="email"> Subject 
                  <i class="red">*</i> 
                  </label>
                  <div class="col-10">      
                     {!! Form::text('template_subject',$arr_data['template_subject'],['class'=>'form-control','data-parsley-required'=>'true','data-parsley-maxlength'=>'255', 'placeholder'=>'Email Template Subject']) !!}  
                  </div>
                  <span class='help-block'> {{ $errors->first('template_subject') }} </span>  
               </div>
               <div class="form-group row">
                  <label class="col-2 col-form-label" for="email">  Body 
                  <i class="red">*</i> 
                  </label>
                  <div class="col-10">   
                     {!! Form::textarea('template_html',$arr_data['template_html'],['class'=>'form-control', 'class'=>'form-control','id' => 'template_html', 'rows'=>'25', 'data-parsley-required'=>'true', 'placeholder'=>'Email Template Body']) !!}  
                     <span class='red'> {{ $errors->first('template_html') }} </span> 
                     <span class="text-info"> Variables </span>
                     
                     @if(sizeof($arr_variables)>0)
                     @foreach($arr_variables as $variable)
                     <br> <label> {{ $variable }} </label> 
                     @endforeach
                     @endif 
                  </div>
               </div>
               <div class="form-group row">
                  <div class="col-10">
                  <input type="hidden" name="enc_id" value="{{base64_encode($arr_data['id'])}}">
                     <button type="button" onclick="saveTinyMceContent();" class="btn btn-success waves-effect waves-light" id="btn_update" value="Update">Update</button>
                     {{-- <button type="submit" class="btn btn-success waves-effect waves-light" value="Update">Update</button> --}}
                     <a class="btn btn-inverse waves-effect waves-light" href="{{$module_url_path}}">Back</a>
                     <a class="btn btn-danger" target="_blank" href="{{ url('/admin/email_template').'/view/'.base64_encode($arr_data['id']) }}"  title="Preview">Preview</a>

                  </div>
               </div>

               {!! Form::close() !!}
            </div>
         </div>
      </div>
   </div>
</div>
</div>
<!-- END Main Content -->
<script type="text/javascript">
   
   
   $(document).ready(function()
   {
      // CKEDITOR.replace( 'template_html' );

     // CKEDITOR.replaceAll();
      
     $('#btn_update').click(function(){

      if($('#validation-form').parsley().validate() == false) return; 

      var formdata = $('#validation-form').serialize();

      $.post('{{url('/admin/email_template/update')}}',formdata,function(data,status){

         swal(data.status,data.description,data.status);

      });

   });
     

   });
</script>
<script type="text/javascript" src="{{url('/assets/js/tinyMCE.js')}}"></script>
<script type="text/javascript">
   function saveTinyMceContent()
   {
     tinyMCE.triggerSave();
   }
</script>
@stop
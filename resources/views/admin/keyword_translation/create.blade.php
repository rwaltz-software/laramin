@extends('admin.layout.master')    
@section('main_content')


<div id="page-wrapper">
<div class="container-fluid">
<div class="row bg-title">
   <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title">{{$page_title or ''}}</h4>
   </div>
   <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
      <ol class="breadcrumb">
         <li><a href="{{url('/')}}/admin/dashboard">Dashboard</a></li>
         <li><a href="{{$module_url_path}}">{{$module_title or ''}}</a></li>
         <li class="active">Add {{$module_title or ''}}</li>
      </ol>
   </div>
   <!-- /.col-lg-12 -->
</div>
<!-- BEGIN Main Content -->
<div class="row">
   <div class="col-md-12">
      <div class="white-box">
         @include('admin.layout._operation_status')  
         <div class="row">
        {{--  <div style="float: right">
          <button type="button" id="append_new_html_data" title="" class="btn btn-success btn-sm show-tooltip" ><i class="fa fa-plus-circle"></i></button></div> --}}
            <div class="col-sm-12 col-xs-12">
                <div class="box-content studt-padding">

                  <form method="POST" id="validation-form" class="form-horizontal" action="{{$module_url_path}}/save" enctype="multipart/form-data">
                        
                        {{ csrf_field() }}

                           <div class="form-group add_data" id="add_data" {{-- style="display:none --}}">
                               <div class="col-sm-6 col-lg-7 controls"></div>
                               <div class="col-sm-12 col-lg-12 control-label" >
                                 <button type="button" id="append_new_html_data" title="" class="btn btn-success btn-sm show-tooltip" style="float:right" ><i class="fa fa-plus-circle"></i></button>
                               </div>
                           </div>
                          
                          
                          @if(isset($arr_lang) && sizeof($arr_lang)>0)                  
                          
                          <input type="hidden" id="arr_lang" name="arr_lang" value="{{ (isset($arr_lang))? json_encode($arr_lang): json_encode(array()) }}">                  
                           
                              <div class="lang_div" > 
                               @foreach($arr_lang as $lang)   
                                    <div class="form-group row">
                                         <label class="col-2 col-form-label">{{isset($lang['title']) ? $lang['title'] : ''}}  @if($lang['locale'] == 'en') <i class="red">*</i> @endif </label>

                                         <div class="col-10">

                                            <input type="text" class="form-control"
                                            @if($lang['locale'] == 'en')  name="english[]" 
                                            @else name="{{isset($lang['title']) ? str_slug($lang['title'],'_') : ''}}[]" 
                                            @endif 
                                            
                                            @if($lang['locale'] == 'en') data-rule-required="true" @endif 
                                            @if($lang['locale'] == 'en') placeholder="Enter Keyword" @endif />

                                            <span class='help-block'>{{ $errors->first(str_slug($lang['title'],'_')) }}</span>  
                                        </div>
                                    
                                    </div>
                                @endforeach
                            </div>

                         {{--    <div class="form-group add_data" id="add_data" style="display:none">
                               <div class="col-sm-6 col-lg-7 controls"></div>
                               <div class="col-sm-12 col-lg-12 control-label" >
                                 <button type="button" id="append_new_html_data" title="" class="btn btn-success btn-sm show-tooltip" style="float:right" ><i class="fa fa-plus-circle"></i></button>
                               </div>
                           </div> --}}
                          
                            @endif                

                            <div class="form-group">
                             <div class="col-sm-9 col-sm-offset-3 col-lg-10 col-lg-offset-2">
                                    <input type="submit" id="save_btn" class="btn btn btn-primary" value="Save "/>
                                    <a class="btn btn-inverse waves-effect waves-light" href="{{$module_url_path}}">Back</a>
                            </div>
                            </div>
                      </form>
                  </div>
            </div>
         </div>
      </div>
   </div>
</div>
  <!-- END Main Content --> 

  
<script type="text/javascript">
  
  $(document).ready(function()
  {  


    $("#autocomplete").bind('keypress',function(event)
    {
    });


   $('#append_new_html_data').click(function()
   { 
      var div =  $('.add_data:last');
      var new_html = '';
      

      var length = $('.lang_div').length;
      
      if(length == 1)
      {
          $('.lang_div').clone().insertAfter(div);

          new_html = '<div class="form-group" >'+
                          '<div class="col-sm-3 col-lg-2 control-label">&nbsp;</div>'+
                          '<div class="col-sm-6 col-lg-6 control-label">'+                       
                            '<button type="button" onclick="removeDynamicKeyword(this)" id="remove_data" title="Remove Keyword" class="btn btn-danger btn-sm show-tooltip" ><i class="fa fa-minus-circle"></i></button>'+
                           '</div> '+
                      '</div>';  
          
          $('.lang_div:last').append(new_html); 
      }
      else
      {
        $('.lang_div:last').clone().insertAfter('.lang_div:last');
      }
      
   }); 

  }); 

function removeDynamicKeyword(ref)
{
    $(ref).parent().parent().parent().remove();
}
  </script>

  @endsection

@extends('admin.layout.master')    
@section('main_content')
<link rel="stylesheet" type="text/css" href="{{url('/')}}/assets/data-tables/latest/dataTables.bootstrap.min.css">
<!-- Page Content -->
<div id="page-wrapper">
<div class="container-fluid">
<div class="row bg-title">
   <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title">{{$module_title or ''}}</h4>
   </div>
   <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
      <ol class="breadcrumb">
         <li><a href="{{url('/')}}/admin/dashboard">Dashboard</a></li>
         <li class="active">{{$module_title or ''}}</li>
      </ol>
   </div>
   <!-- /.col-lg-12 -->
</div>
<!-- BEGIN Main Content -->
<div class="row">
   <div class="col-md-12">
      <div class="white-box">
         {{-- @include('admin.layout._operation_status') --}}
         <form class="form-horizontal" id="frm_manage" method="POST" action="{{ url($module_url_path.'/multi_action') }}">
            {{ csrf_field() }}
            <div class="pull-right">
              
              <a href="http://localhost/laramin/admin/keyword_translation/create" class="btn btn-outline btn-warning btn-circle show-tooltip" title="Add New User"><i class="fa fa-plus"></i> </a>

               <a href="javascript:void(0)" onclick="javascript:location.reload();" class="btn btn-outline btn-info btn-circle show-tooltip" title="Refresh"><i class="icon-refresh"></i> </a> 

            </div>
           
            <div class="row">
               <div class="block-new-block">
                  <!--<div class="col-sm-4 col-lg-3 controls">
                     <a href="{{ url($module_url_path.'/create') }}" title="Add {{ $module_title or ''}}">
                       <button type="button" class="btn btn-success">
                           <i class="fa fa-plus" aria-hidden="true"></i> 
                              Add {{ $module_title or ''}}
                       </button>
                     </a>
                     </div>-->
               </div>
            </div>
            <div class="table-responsive">
               <table class="table table-striped"   id="table_module">
                  <thead>
                     <tr>
                        <th>
                        <a class="sort-desc sort-active" >Keyword </a>
                        </th>
                        <th><a class="sort-asc" >Title</a>
                        </th>
                        <th>
                           <a class="sort-desc" >Locale</a>
                        </th>
                        <th><a class="sort-desc sort-asc">Action</a></th>
                     </tr>
                     <tr>
                       <td><input type="text" name="q_keyword" placeholder="Search" class="search-block-new-table column_filter form-control" /></td>
                       <td><input type="text" name="q_title" placeholder="Search" class="search-block-new-table column_filter form-control" /></td>
                       <td>
                         @if(isset($arr_lang) && sizeof($arr_lang) > 0)
                           <select name="q_locale" class="search-block-new-table column_filter form-control">
                              <option value="">Select</option>
                              @foreach($arr_lang as $lang)
                              <option value="{{isset($lang['locale']) ? $lang['locale'] : ''}}">{{isset($lang['title']) ? $lang['title'] : ''}}  </option>
                              @endforeach
                           </select>
                           @endif
                       </td>
                       <td></td>
                     </tr>
                  </thead>
               </table>
            </div>
            <div> 
            </div>
         </form>
      </div>
   </div>
</div>
<!-- END Main Content -->
<script type="text/javascript">
   function show_details(url)
   {
     window.location.href = url;
   }
   
   var table_module = false;
   $(document).ready(function()
   {
     table_module = $('#table_module').DataTable({ 
       processing: true,
       serverSide: true,
       autoWidth: false,
       bFilter: false ,
       ajax: {
       'url':'{{ $module_url_path.'/get_records'}}',
       'data': function(d)
         {
   
           d['column_filter[q_keyword]']        = $("input[name='q_keyword']").val()
           d['column_filter[q_title]']          = $("input[name='q_title']").val()
           d['column_filter[q_locale]']         = $("select[name='q_locale']").val()         
         }
       },
       columns: [
       
       
       {data: 'keyword', "orderable": true, "searchable":false},
       {data: 'title', "orderable": true, "searchable":false},
       {data: 'locale', "orderable": false, "searchable":false},      
       
       
       {
         render : function(data, type, row, meta) 
         {
           return '<a class="btn btn-outline btn-info btn-circle show-tooltip" href="'+row.built_edit_href+'" title="Edit"><i class="ti-pencil-alt2" ></i></a>&nbsp;';
         },
         "orderable": false, "searchable":false
       }]
     });
     
   
     $('input.column_filter').on( 'keyup click', function () 
     {
         filterData();
     });
   
     $('select.column_filter').on( 'keyup click', function () 
     {
         filterData();
     });
   
     $('#table_module').on('draw.dt',function(event)
     {
       var oTable = $('#table_module').dataTable();
       var recordLength = oTable.fnGetData().length;
       $('#record_count').html(recordLength);
     });
   });
   
   
   
   function filterData()
   {
     table_module.draw();
   }
   
   
</script>
@stop
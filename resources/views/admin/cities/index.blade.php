@extends('admin.layout.master')                
@section('main_content')
<!-- BEGIN Page Title -->
<link rel="stylesheet" type="text/css" href="{{url('/')}}/assets/data-tables/latest/dataTables.bootstrap.min.css">
<!-- Page Content -->
<div id="page-wrapper">
<div class="container-fluid">
<div class="row bg-title">
   <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
      <h4 class="page-title">{{$module_title or ''}}</h4>
   </div>
   <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
      <ol class="breadcrumb">
         <li><a href="{{url('/')}}/admin/dashboard">Dashboard</a></li>
         <li class="active">{{$module_title or ''}}</li>
      </ol>
   </div>
   <!-- /.col-lg-12 -->
</div>
<div class="row">
   <div class="col-sm-12">
      <div class="white-box">
         {{-- @include('admin.layout._operation_status')  --}}
	         {!! Form::open([ 'url' => $module_url_path.'/multi_action',
	         'method'=>'POST',
	         'enctype' =>'multipart/form-data',   
	         'class'=>'form-horizontal', 
	         'id'=>'frm_manage' 
	         ]) !!} 
	         {{ csrf_field() }}
          <div class="pull-right">

            <a href="{{ url($module_url_path.'/create') }}" class="btn btn-outline btn-info btn-circle show-tooltip" title="Add More"><i class="fa fa-plus"></i> </a> 
            <a href="javascript:void(0);" onclick="javascript : return check_multi_action('checked_record[]','frm_manage','activate');" class="btn btn-circle btn-success btn-outline show-tooltip" title="Multiple Unlock"><i class="ti-unlock"></i></a> 
            <a  href="javascript:void(0);" onclick="javascript : return check_multi_action('checked_record[]','frm_manage','deactivate');" class="btn btn-circle btn-danger btn-outline show-tooltip" title="Multiple Lock"><i class="ti-lock"></i> </a> 
            <a  href="javascript:void(0);" onclick="javascript : return check_multi_action('checked_record[]','frm_manage','delete');" class="btn btn-circle btn-danger btn-outline show-tooltip" title="Multiple Delete"><i class="ti-trash"></i> </a> 
            <a href="javascript:void(0)" onclick="javascript:location.reload();" class="btn btn-outline btn-info btn-circle show-tooltip" title="Refresh"><i class="fa fa-refresh"></i> </a>

					<br><br>
                   </div>
            <div class="table-responsive">
                  
                   <input type="hidden" name="user_id" id="user_id">
                   <input type="hidden" name="country_id" id="country_id">
                   
                    <table id="myTable" class="table table-striped">
                        <thead>
                            <tr> 
                                <th> 
                                <div class="checkbox checkbox-success">
                                    <input class="checkboxInputAll" id="checkbox0" type="checkbox">
                                    <label for="checkbox0">  </label>
                                </div>
                                </th>
                                <th>ID</th>
                                <th>Name</th>
                                <th>state</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        
                    </table>
                </div>
            <input type="hidden" name="multi_action" value="" />
          {!! Form::close() !!}
      </div>
  </div>
</div>

<!-- END Main Content -->
<script type="text/javascript">
       
      $(document).ready(function() 
      {
      // alert(3434);
        var site_url = '{{ url('/') }}';
       
          $('#myTable').DataTable({
             processing: true,
             serverSide: true,
             responsive:true,
               searchable: false,
             ajax: site_url+'/admin/cities/get_city/',
             columns: [
	                  {
	                    render : function(data, type, row, meta) 
	                    {
	                      // console.log(row);
	                    // return '<input type="checkbox" '+
	                    // ' name="checked_record[]" '+  
	                    // ' value="'+row.id+'" class="case checkboxInput" id="checked_record"   data-checkbox="icheckbox_flat-red" />';
                       return `<div class="checkbox checkbox-success"><input type="checkbox" name="checked_record[]" value="`+row.id+`" id="checkbox`+row.id+`" class="case checkboxInput"/><label for="checkbox`+row.id+`"></label> </div>`;
	                    },
	                    "orderable": false,
	                    "searchable":false
	                   },
	                   {data: 'id'},
	                   {data: 'name',responsivePriority:1},
	                   // {data: 'state_id'},
	                   // {data: 'is_active'},
                      {
                      render : function(data, type, row, meta) 
                      { 
                        // console.log(row.name);
                        return row.name;
                      }  
                     },
	                   {
	                    data : 'is_active',  
	                    render : function(data, type, row, meta) 
	                    {
	                      if(row.is_active == '1')
	                      {
	                        return `<input type="checkbox" checked data-size="small"  data-enc_id="`+btoa(row.id)+`"  id="status_`+row.id+`" class="js-switch toggleSwitch" onchange='statusChange($(this))'   data-type="deactivate" data-color="#99d683" data-secondary-color="#f96262" />`
	                      }
	                      else
	                      {
	                        return `<input type="checkbox" data-size="small" data-enc_id="`+btoa(row.id)+`"  class="js-switch toggleSwitch" onchange='statusChange($(this))' data-type="activate" data-color="#99d683" data-secondary-color="#f96262" />`

	                      }
	                    },
	                    "orderable": false,
	                    "searchable":false
	                   }, 
	                    {data: 'action',
	                    name: 'action', 
	                    orderable: false, 
	                    searchable: false,
	                    responsivePriority:4 , 
	                    render : function(data,type,row,meta)
	                    {
	                         return `<a href="{{ $module_url_path.'/show/'}}`+btoa(row.id)+`"  data-size="small" class="btn btn-outline btn-primary btn-circle show-tooltip" title="Add"><i class="fa fa-eye"></i></a> 

	                                 <a href="{{ $module_url_path.'/edit/'}}`+btoa(row.id)+`"  data-size="small" class="btn btn-outline btn-success btn-circle show-tooltip" title="Edit"><i class="ti-pencil-alt2"></i></a> 

	                                <a href="{{ $module_url_path.'/delete/'}}`+btoa(row.id)+`"  data-size="small" class="btn btn-outline btn-danger btn-circle show-tooltip" title="Delete" onclick="confirm_action(this,event,'Are you sure to delete this record?')"><i class="ti-trash"></i></a> 
	                                 `;
	                    } 
	                  } 
                ]
          });
           $('#myTable').on('draw.dt',function(event){
            statusemoji();
            });
		          
      });
      function statusemoji()
      {
           // Switchery
          var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
          $('.js-switch').each(function() {
              new Switchery($(this)[0], $(this).data());
          });
      }

    function show_details(url)
    { 
       
        window.location.href = url;
    } 

    $(function(){
     $("input.checkboxInputAll").click(function(){

        if($("input.checkboxInput:checkbox:checked").length <= 0){
            $("input.checkboxInput").prop('checked',true);
        }else{
            $("input.checkboxInput").prop('checked',false);
        }

       }); 
    });
      
   function statusChange(data)
   {
  	 var module_url_path = '{{$module_url_path}}';
     var ref = data; 
     var type = data.attr('data-type');
     var enc_id = data.attr('data-enc_id');
     var id = data.attr('data-id');

       $.ajax({
         url:module_url_path+'/'+type,
         type:'GET',
         data:{id:enc_id},
         dataType:'json',
         success: function(response)
         {
           if(response.status=='SUCCESS'){
             if(response.data=='ACTIVE')
             {
               $(ref)[0].checked = true;  
               $(ref).attr('data-type','deactivate');

             }else
             {
               $(ref)[0].checked = false;  
               $(ref).attr('data-type','activate');
             }
           }
           else
           {
             sweetAlert('Error','Something went wrong!','error');
           }  
         }
       });  
   } 


</script>

@stop                    



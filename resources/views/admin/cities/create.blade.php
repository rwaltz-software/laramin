@extends('admin.layout.master')                
@section('main_content')
<!-- Page Content -->
<div id="page-wrapper">
<div class="container-fluid">
   <div class="row bg-title">
      <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
         <h4 class="page-title">{{$page_title or ''}}</h4>
      </div>
      <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
         <ol class="breadcrumb">
            <li><a href="{{url('/')}}/admin/dashboard">Dashboard</a></li>
            <li><a href="{{$module_url_path}}">{{$module_title or ''}}</a></li>
            <li class="active">{{$page_title or ''}}</li>
         </ol>
      </div>
      <!-- /.col-lg-12 -->
   </div>
   <!-- .row -->
   <div class="row">
      <div class="col-sm-12">
         <div class="white-box">
            @include('admin.layout._operation_status')
              <div class="tabbable">
                <ul id="myTab1" class="nav nav-tabs">
                  @if(isset($arr_lang) && sizeof($arr_lang)>0)
                  @foreach($arr_lang as $lang)
                  <li class="{{ $lang['locale']=='en'?'active':'' }}">
                    <?php 
                    $is_linked_enabled = $lang['locale']=='en'?TRUE:FALSE;
                    ?>
                    <a href="#{{$lang['locale']}}" 
                    {{ $lang['locale']=='en'?'data-toggle="tab"':'' }} >
                    <i class="fa fa-home"></i> 
                    {{$lang['title']}} 
                  </a>
                </li>
                @endforeach
                @endif
              </ul>

              {!! Form::open([ 
                              'method'=>'POST',
                              'enctype' =>'multipart/form-data',   
                              'class'=>'form-horizontal', 
                              'id'=>'validation-form' 
              ]) !!}              

              {{ csrf_field() }}  

              <div id="myTabContent1" class="tab-content">

               @if(isset($arr_lang) && sizeof($arr_lang)>0)
               @foreach($arr_lang as $lang)

               <div class="tab-pane fade {{ $lang['locale']=='en'?'in active':'' }}" 
               id="{{ $lang['locale'] }}">

              @if($lang['locale']=="en")
                <div class="form-group row">
                    <label class="col-2 col-form-label" for="country_code">Country <i class="red">*</i></label>
                    <div class="col-10">
                      @if($lang['locale'] == 'en') 
                      {!!
                          Form::select('country_id',$arr_country,"",['class'=>'form-control','data-parsley-required'=>'true', 'onchange'=>'loadStates(this)'])
                      !!}
                    @endif
                        
                        <span class='red'>{{ $errors->first('country_id') }}</span>
                    </div>
                </div>

                 <div class="form-group row">
                  <label class="col-2 col-form-label">State/Region<i class="red">*</i></label>
                  <div class="col-10">
                  
                    @if($lang['locale'] == 'en') 
                      {!!
                          Form::select('state_id', ['0' => 'Select'],"",['class'=>'form-control','data-parsley-required'=>'true','required'])
                      !!}
                    @endif
                       <span class='red'>{{ $errors->first('state_id') }}</span>
                  </div>
               </div>

              @endif
               
               <div class="form-group row">
                <label class="col-2 col-form-label" >City<i class="red">*</i></label>
                <div class="col-10">
                  @if($lang['locale'] == 'en')        
                    {!! Form::text('city_title_'.$lang['locale'],old('city_title_'.$lang['locale']),['class'=>'form-control','data-parsley-required'=>'true','placeholder'=>'City Title']) !!}
                    @else
                    {!! Form::text('city_title_'.$lang['locale'],old('city_title_'.$lang['locale']),['class'=>'form-control','placeholder'=>'City Title']) !!}
                  @endif
                  <span class='red'>{{ $errors->first('city_title_'.$lang['locale']) }}</span>
                </div>
              </div>         

            </div>
            @endforeach
            @endif
          </div>

          <br>
          <div class="form-group row">
            <div class="col-10">
             {{-- <button class="btn btn-success waves-effect waves-light m-r-10" type="submit" name="Save" value="Save"> Save</button> --}}
             <button class="btn btn-success waves-effect waves-light m-r-10" type="button" id="btn_add" name="Save" value="Save"> Save</button>
                     <a class="btn btn-inverse waves-effect waves-light" href="{{$module_url_path}}">Back</a>
            </div>
          </div>
        {!! Form::close() !!}
      </div>
      
      
    </div>
  </div>
  </div>

  <!-- END Main Content -->

<script type="text/javascript">

    var url = "{{ url('/') }}";
    function loadStates(ref)
     {
        var selected_country = $(ref).val();

        $.ajax({
                        url:url+'/admin/common/get_states/'+selected_country,
                        type:'GET',
                        data:'flag=true',
                        dataType:'json',
                        beforeSend:function()
                        {
                            $('select[name="state"]').attr('disabled','disabled');
                        },
                        success:function(response)
                        {
                            if(response.status=="SUCCESS")
                            {
                              
                                $('select[name="state_id"]').removeAttr('disabled');
                                if(typeof(response.arr_state) == "object")
                                {
                                   var option = '<option value="">Please Select</option>'; 
                                   $(response.arr_state).each(function(index,states)
                                   {   
                                    // console.log(states.id);
                                        option+='<option value="'+states.id+'">'+states.name+'</option>';
                                   });

                                   $('select[name="state_id"]').html(option);
                                }
                            }
                            else
                            {
                              var option = '<option value="">Please Select</option>'; 
                              $('select[name="state_id"]').html(option);
                            }
                            return false;
                        },
                        error:function(response)
                        {
                         
                        }
        });
     }  

     
</script>

    <script type="text/javascript">
      $(document).ready(function(){

      $('#btn_add').click(function(){

       if($('#validation-form').parsley().validate() == false) return ;
       
       var formdata = $('#validation-form').serialize();

        $.post('{{url('admin/cities/save')}}',formdata,function(data,status){

            if('success' == data.status)
            {
              $('#validation-form')[0].reset();

              swal({
                  title: data.status,
                  text: data.description,
                  type: data.status,
                  confirmButtonText: "OK",
                  closeOnConfirm: false
                },
                function(isConfirm,tmp)
                {
                  if(isConfirm==true)
                  {
                    window.location = data.link;
                  }
                });
            }
            else
            {
              swal(data.status,data.description,data.status);
            }
         });

      }); 
   });

     
    </script>


@stop                    

@extends('admin.layout.master')                
@section('main_content')
<!-- Page Content -->
<div id="page-wrapper">
   <div class="container-fluid">
      <div class="row bg-title">
         <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
            <h4 class="page-title">{{$page_title or ''}}</h4>
         </div>
         <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
            <ol class="breadcrumb">
               <li><a href="{{url('/')}}/admin/dashboard">Dashboard</a></li>
               <li><a href="{{$module_url_path}}">{{$module_title or ''}}</a></li>
               <li class="active">{{$page_title or ''}}</li>
            </ol>
         </div>
      </div>
      <!-- BEGIN Main Content -->
      <div class="white-box tabbable">
         @include('admin.layout._operation_status')  
         <ul id="myTab1" class="nav nav-tabs">
            @if(isset($arr_lang) && sizeof($arr_lang)>0)
            @foreach($arr_lang as $lang)
            <li class="{{ $lang['locale']=='en'?'active':'' }}">
               <?php 
                  $is_linked_enabled = $lang['locale']=='en'?TRUE:FALSE;
                  ?>
               <a href="#{{$lang['locale']}}" 
                  data-toggle="tab">
               <i class="fa fa-home"></i> 
               {{$lang['title']}} 
               </a>
            </li>
            @endforeach
            @endif
         </ul>
         {!! Form::open([ 
                         'method'=>'POST',
                         'enctype' =>'multipart/form-data',   
                         'class'=>'form-horizontal', 
                         'id'=>'validation-form' 
         ]) !!} 

         {{ csrf_field() }}  

         <div id="myTabContent1" class="tab-content">
            @if(isset($arr_lang) && sizeof($arr_lang)>0)
            @foreach($arr_lang as $lang)
            <?php                          
               /* Locale Variable */  
               $locale_title = "";                          
               if(isset($arr_data['translations'][$lang['locale']]))
               {
                   $locale_title = $arr_data['translations'][$lang['locale']]['city_title'];
               }
               
               
               ?>
            <div class="tab-pane fade {{ $lang['locale']=='en'?'in active':'' }}" 
               id="{{ $lang['locale'] }}">

               @if($lang['locale']=="en")
                <div class="form-group row">
                  <label class="col-2 col-form-label" for="country_id">Country <i class="red">*</i></label>
                  <div class="col-10">
                     @if($lang['locale'] == 'en')        
                     {!! Form::text('country',$arr_data['country_details']['country_name'],['class'=>'form-control','data-parsley-required'=>'true','readonly'=>'readonly']) !!}
                     @endif
                     <span class='red'>{{ $errors->first('country_id') }}</span>
                  </div>
               </div>
               <div class="form-group row">
                  <label class="col-2 col-form-label" for="state">State<i class="red">*</i></label>
                    <div class="col-10">
                  
                   {{--  @if($lang['locale'] == 'en') 
                      {!!
                          Form::select('state_id', ['0' => 'Select'],"",['class'=>'form-control','data-parsley-required'=>'true'])
                      !!}
                    @endif --}}
                     <select name="state" id="state" onchange="getCity()" class="form-control">
                                          
                       {{-- <option selected disabled>--select--</option> --}}
                            @foreach($states as $statesKey => $statesValues)
                             
                               <option class="form-control" <?php if(isset($arr_data['state']) &&  $statesValues['id'] == $arr_data['state']) {echo 'selected=selected'; }  ?> value="{{$statesValues['id']}}"  >{{$statesValues['name']}}</option>

                            @endforeach
                            
                       </select>

                       <span class='red'>{{ $errors->first('state_id') }}</span>
                  </div>
               </div>

               @endif 
               <div class="form-group row">
                  <label class="col-2 col-form-label" >City<i class="red">*</i></label>
                  <div class="col-10">
                     @if($lang['locale'] == 'en')        
                     {!! Form::text('city_title_'.$lang['locale'],$arr_data['name'],['class'=>'form-control','data-parsley-required'=>'true','placeholder'=>'City Title']) !!}
                     @else
                     {!! Form::text('city_title_'.$lang['locale'],$locale_title,['class'=>'form-control','placeholder'=>'City Title']) !!}
                     @endif                  
                  </div>
               </div>
            </div>
            @endforeach
            @endif
         </div>
         <br>
         <div class="form-group row">
            <div class="col-10">
            <input type="hidden" name="city_id" value="{{$enc_id}}">
            <input type="hidden" name="country_id" value="{{$arr_data['country_details']['id']}}">
            <input type="hidden" name="state_id" value="{{$arr_data['state_details']['id']}}">
               {{-- <button type="submit" class="btn btn-success waves-effect waves-light m-r-10" value="Update">Update</button> --}}
               <button type="button" class="btn btn-success waves-effect waves-light m-r-10" id="btn_update" value="Update">Update</button>
               <a class="btn btn-inverse waves-effect waves-light" href="{{$module_url_path}}">Back</a>
            </div>
         </div>
         {!! Form::close() !!}
      </div>
   </div>
</div>
</div>
<!-- END Main Content -->
<script type="text/javascript">
      $(document).ready(function(){

      $('#btn_update').click(function(){

       if($('#validation-form').parsley().validate() == false) return ;
       
       var formdata = $('#validation-form').serialize();
       
       $.post('{{url('admin/cities/save')}}',formdata,function(data,status){
           // console.log(data.status);
            swal(data.status,data.description,data.status);
         });

      }); 
   });
    </script>
@stop
<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Datatables;
use App\Models\CountryModel;
use App\Models\StateModel; 
use App\Models\StateTranslationModel;
use Validator;
use Session;
use App\Common\Services\LanguageService;
use App\Common\Traits\MultiActionTrait;
use Flash;
 
class StateController extends Controller
{

    use MultiActionTrait;
    public function __construct(CountryModel $countries, StateModel $state, LanguageService $langauge,StateTranslationModel $StateTranslationModel)
    {
        $this->CountryModel     = $countries;
        $this->StateModel       = $state;
        $this->LanguageService  = $langauge;
        $this->StateTranslationModel  = $StateTranslationModel;

        $this->BaseModel = $this->StateModel;

        $this->arr_view_data      = [];
        $this->module_url_path    = url(config('app.project.admin_panel_slug')."/states");
        $this->module_title       = "States";
        $this->module_view_folder = "admin.states";
    } 

    public function index()
    {
        $arr_view_data['module_url_path'] = $this->module_url_path;
        $arr_view_data['module_title'] = $this->module_title;
        return view($this->module_view_folder.'.index',$arr_view_data);
    }

    public function get_state()
    {      

        $state_details = $this->StateModel->with(['country_details'=>function($query){ 
                                                    return $query->select('id','country_name');
                                                }]); 


        return Datatables::of($state_details)->make(true);
    }


    // public function index()
    // {
    //     $this->arr_view_data['arr_data'] = array();

    //     $obj_data = $this->BaseModel->with(['country_details'])->get();

    //     $arr_lang =  $this->LanguageService->get_all_language();       

    //     if(sizeof($obj_data)>0)
    //     {
    //         foreach ($obj_data as $key => $data) 
    //         {
    //             $arr_tmp = array();
    //             /* Check Language Wise Transalation Exists*/
    //             foreach ($arr_lang as $key => $lang) 
    //             {
    //                 $arr_tmp[$key]['title'] = $lang['title'];
    //                 $arr_tmp[$key]['is_avail'] = $data->hasTranslation($lang['locale']);
    //             }    

    //             $data->arr_translation_status = $arr_tmp;

    //             /* Call to hasTranslation method of object is triggering translations so need to unset it */
    //             unset($obj_data->translations);
    //         }  

    //         $this->arr_view_data['arr_data'] = $obj_data->toArray();
    //     }

    //     $this->arr_view_data['page_title']      = "Manage ".str_singular($this->module_title);
    //     $this->arr_view_data['module_title']    = str_plural($this->module_title);
    //     $this->arr_view_data['module_url_path'] = $this->module_url_path;
    //     $this->arr_view_data['arr_lang']        = $arr_lang;
        
    //     return view($this->module_view_folder.'.index', $this->arr_view_data);
    // }

    public function show($enc_id)
    {
        $id = base64_decode($enc_id);

        $arr_data = array();
        $obj_data = $this->BaseModel->where('id',$id)->with(['country_details'])->first();
       
        if($obj_data)
        {
            $arr_data = $obj_data->toArray();
        }

        $this->arr_view_data['arr_data']        = $arr_data;
        $this->arr_view_data['page_title']      = "Show ".str_singular($this->module_title);
        $this->arr_view_data['module_title']    = str_plural($this->module_title);
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view($this->module_view_folder.'.show', $this->arr_view_data);
    }
    
    public function create()
    {
        $arr_country = array();

        $obj_country = $this->CountryModel->where('is_active','1')->get();
        // dd($obj_country);
       
        if( $obj_country != FALSE)
        {
            $arr_country = $obj_country->toArray();
        }

        $arr_default = array();

        $this->arr_view_data['arr_country']     = $this->build_select_options_array($arr_country,'id','country_name',$arr_default);
        $this->arr_view_data['arr_lang']        = $this->LanguageService->get_all_language();
        $this->arr_view_data['page_title']      = "Create ".str_singular($this->module_title);
        $this->arr_view_data['module_title']    = str_plural($this->module_title);
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        
        return view($this->module_view_folder.'.create', $this->arr_view_data);
    }

    public function edit($enc_id)
    {
        $id = base64_decode($enc_id);

        $arr_default = array();

        $obj_data = $this->BaseModel
                          ->where('id',$id)
                          ->with(['country_details','translations'])
                          ->first();

        $arr_data = [];
        if($obj_data)
        {
           $arr_data = $obj_data->toArray(); 
           /* Arrange Locale Wise */
           $arr_data['translations'] = $this->arrange_locale_wise($arr_data['translations']);
        }

        $arr_country = array();
        $obj_country = $this->CountryModel->where('is_active','1')->get();
       
        if( $obj_country != FALSE)
        {
            $arr_country = $obj_country->toArray();
        }

        $arr_default = array();
        $this->arr_view_data['arr_country'] = $this->build_select_options_array($arr_country,'id','country_name',$arr_default);

        $this->arr_view_data['edit_mode'] = TRUE;
        $this->arr_view_data['enc_id']    = $enc_id;
        $this->arr_view_data['arr_lang']  = $this->LanguageService->get_all_language();   
        
        $this->arr_view_data['arr_data']        = $arr_data;
        $this->arr_view_data['page_title']      = "Edit ".str_singular($this->module_title);
        $this->arr_view_data['module_title']    = str_plural($this->module_title);
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
     
        return view($this->module_view_folder.'.edit', $this->arr_view_data);    
    }

    public function save(Request $request)
    {  
       $is_update = false;

       $form_data = $request->all();  

       $state_id  = base64_decode($request->input('enc_id',false));

       $state_id = $state_id ==" "   ? false : $state_id;

       if($request->has('enc_id'))
       {
         $is_update = true;
       }

       $arr_rules = [
                       'state_title_en'=>'required',
                       'country_id'=>'required'
                    ];

        $validator = Validator::make($request->all(),$arr_rules);
 
        if($validator->fails())
        {
            $response['status']      = 'warning';
            $response['description'] = 'Form Validation Failed Please Check Form Field';

            return response()->json($response);
        }

       $is_duplicate = $this->BaseModel->where('country_id', $request->input('country_id'))
                      ->whereHas('translations',function($query) use($request)
                         {
                              $query->where('locale', 'en')
                                    ->where('name',$request->input('state_title_en'));
                         });
                            
       if($is_update)
       {
         $is_duplicate = $is_duplicate->where('id','<>',$state_id);
       }

       $does_exists = $is_duplicate->count();
       
       if($does_exists)
       {
        $response['status'] = 'warning';
        $response['description'] = str_singular($this->module_title).' Already Exists.';

        return response()->json($response);
       }
        // Insert Into State Table
       $state = $this->StateModel->firstOrNew(['id' => $state_id]);

       $state->country_id = $form_data['country_id'];
       $state->name       = $form_data['state_title_en'];
       $state_details     = $state->save();

       if($state_details)
       {  
            /*----------------------------------------------------------------------*/
         $arr_lang =  $this->LanguageService->get_all_language();
        
         if(sizeof($arr_lang) > 0 )
         {
            foreach ($arr_lang as $lang) 
            {
                $state_title = $form_data['state_title_'.$lang['locale']];

                if( isset($state_title) && $state_title != "")
                { 
                   $translation = $state->getTranslation($lang['locale']);  

                    if($translation)
                    {
                        $translation->name =  $state_title;
                        // $translation->state_slug  =  str_slug($request->input('state_title_'.$lang['locale']), "-");
                        $translation->save();    
                     }  
                    else
                    {
                        /* Create New Language Entry  */
                        $translation = $state->translateOrNew($lang['locale']);

                        $translation->state_id     = $state->id;
                        $translation->name         = $state_title;
                        $translation->state_slug   = str_slug($state_title, "-");

                        if($is_update == false)
                        {
                              $translation->state_slug   = str_slug($form_data['state_title_en'], "-");
                        }
                        
                        $translation->save();
                    }

                    // Flash::success(str_singular($this->module_title).' Save Successfully');
                }
            }//foreach
         } //if
       
        /*-------------------------------------------------------
         |   Activity log Event
            --------------------------------------------------------*/
            $arr_event                 = [];
            $arr_event['ACTION']       = 'ADD';

            if($is_update)
            {
               $arr_event['ACTION']       = 'EDIT';
            }

            $arr_event['MODULE_TITLE'] = $this->module_title;

            $this->save_activity($arr_event);

            $response['status'] = 'success';
            $response['description'] = str_singular($this->module_title).'Save Successfully';

            if($is_update == false)
            {
              if($state->id)
              {
                $response['link'] = url('/admin/states/edit/'.base64_encode($state->id));
              }
            }  
       }
       else
       {

        $response['status'] = 'error';
        $response['description'] ='Error Occurred While Save'.str_singular($this->module_title).'.';
        
       }
       return response()->json($response);
   }
    
    public function arrange_locale_wise(array $arr_data)
    {
        if(sizeof($arr_data)>0)
        {
            foreach ($arr_data as $key => $data) 
            {
                $arr_tmp = $data;
                unset($arr_data[$key]);

                $arr_data[$data['locale']] = $data;                    
            }

            return $arr_data;
        }
        else
        {
            return [];
        }
    }

    public function activate(Request $request)
    {
        $enc_id = $request->input('id');

        if(!$enc_id)
        {
            return redirect()->back();
        }

        $arr_response = [];    
        if($this->perform_activate(base64_decode($enc_id)))
        {
            $arr_response['status'] = 'SUCCESS';
        }
        else
        {
            $arr_response['status'] = 'ERROR';
        }

        $arr_response['data'] = 'ACTIVE';

        return response()->json($arr_response);
    }

    public function deactivate(Request $request)
    {
        $enc_id = $request->input('id');

        if(!$enc_id)
        {
            return redirect()->back();
        }
        $arr_response = []; 

        if($this->perform_deactivate(base64_decode($enc_id)))
        {
             $arr_response['status'] = 'SUCCESS';
        }
        else
        {
            $arr_response['status'] = 'ERROR';
        }

        $arr_response['data'] = 'DEACTIVE';

        return response()->json($arr_response);
    }

    public function perform_activate($id)
    {
        $activate = $this->BaseModel->where('id',$id)->update(['is_active'=>'1']);
        
        if($activate)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function perform_deactivate($id)
    {
        $deactivate     = $this->BaseModel->where('id',$id)->update(['is_active'=>'0']);
        if($deactivate)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function delete($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            return redirect()->back();
        }

        if($this->perform_delete(base64_decode($enc_id)))
        {
            Flash::success(str_singular($this->module_title).' Deleted Successfully');
        }
        else
        {
            Flash::error('Problem Occured While '.str_singular($this->module_title).' Deletion ');
        }

        return redirect()->back();
    }

     /*
    | multi_action() : mutiple actions like active/deactive/delete for multiple slected records
    | auther : MOHAN SONAR 
    | Date : 01-02-2016    
    | @param  \Illuminate\Http\Request  $request
    */
    // public function multi_action(Request $request)
    // {
    //     $arr_rules = array();
    //     $arr_rules['multi_action'] = "required";
    //     $arr_rules['checked_record'] = "required";


    //     $validator = Validator::make($request->all(),$arr_rules);

    //     if($validator->fails())
    //     {
    //         return redirect()->back()->withErrors($validator)->withInput();
    //     }

    //     $multi_action = $request->input('multi_action');
    //     $checked_record = $request->input('checked_record');

    //     /* Check if array is supplied*/
    //     if(is_array($checked_record) && sizeof($checked_record)<=0)
    //     {
    //         Session::flash('error','Problem Occured, While Doing Multi Action');
    //         return redirect()->back();

    //     }

        
    //     foreach ($checked_record as $key => $record_id) 
    //     {  
    //         if($multi_action=="delete")
    //         {
    //            $this->perform_delete(base64_decode($record_id));    
    //            Flash::success(str_plural($this->module_title).' Deleted Successfully'); 
    //         } 
    //         elseif($multi_action=="activate")
    //         {
    //            $this->perform_activate(base64_decode($record_id)); 
    //            Flash::success(str_plural($this->module_title).' Activated Successfully'); 
    //         }
    //         elseif($multi_action=="deactivate")
    //         {
    //            $this->perform_deactivate(base64_decode($record_id));    
    //            Flash::success(str_plural($this->module_title).' Blocked Successfully');  
    //         }
    //     }

    //     return redirect()->back();
    // }

    

    

    public function perform_delete($id)
    {
        $entity = $this->BaseModel->where('id',$id)->first();
        if($entity)
        {
            /*-------------------------------------------------------
            |   Activity log Event
            --------------------------------------------------------*/
                $arr_event                 = [];
                $arr_event['ACTION']       = 'REMOVED';
                $arr_event['MODULE_TITLE'] = $this->module_title;

                $this->save_activity($arr_event);

            /*----------------------------------------------------------------------*/

            return $entity->delete();
        }

        return FALSE;
    }

    public function build_select_options_array(array $arr_data,$option_key,$option_value,array $arr_default)
    {

        $arr_options = [];
        if(sizeof($arr_default)>0)
        {
            $arr_options =  $arr_default;   
        }

        if(sizeof($arr_data)>0)
        {
            foreach ($arr_data as $key => $data) 
            {
                if(isset($data[$option_key]) && isset($data[$option_value]))
                {
                    $arr_options[$data[$option_key]] = $data[$option_value];
                }
            }
        }

        return $arr_options;
    }

    public function insert_records()
    {
        $state_obj = $this->StateModel->get();

        if($state_obj)
        {
            $state_arr = $state_obj->toArray();
        }
         // $count = count($country_arr);
        foreach($state_arr as $k => $v)
        {
          // echo $v['id'].' '.$v['name'].'<br>';

          $data['id']           = $v['id'];
          $data['name']         = $v['name'];
          $data['state_id']     = $v['id'];
          $data['state_slug']   = str_slug($v['name'], '-');
          $data['locale']       = 'en';

          $insert_states = $this->StateTranslationModel ->create($data);

        }
        

        if($insert_states)
        {
            dd(33333333);
            echo 'inserted';
        }
        else
        {
            dd(444444);
            echo "failed";
        }
    }
}

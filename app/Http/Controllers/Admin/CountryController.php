<?php

namespace App\Http\Controllers\Admin;
use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Common\Traits\MultiActionTrait;

use App\Models\CountryModel;
use App\Models\CountryTranslationModel;
use Validator;
use Session;
use App\Common\Services\LanguageService;
use Flash;
 
class CountryController extends Controller
{
    use MultiActionTrait;
    
    public function __construct(CountryModel $countries, 
                              LanguageService $langauge,
                              CountryTranslationModel $CountryTranslationModel
                              )
    {
        $this->CountryModel    = $countries;
        $this->LanguageService = $langauge;
        $this->BaseModel       = $this->CountryModel;
        $this->arr_view_data   = []; 
        $this->module_url_path    = url(config('app.project.admin_panel_slug')."/countries");
        $this->module_title       = "Countries";
        $this->module_url_slug    = "countries";
        $this->module_view_folder = "admin.countries";
        $this->CountryTranslationModel     = $CountryTranslationModel;
    }   

    public function index()
    {
        $arr_lang = array();
        $arr_data = array();

        $obj_data = $this->BaseModel->get();
        if($obj_data != FALSE)
        {
            $arr_data = $obj_data->toArray();
        }

        $this->arr_view_data['arr_data'] = $arr_data;

        $this->arr_view_data['page_title']      = "Manage ".str_singular($this->module_title);
        $this->arr_view_data['module_title']    = str_plural($this->module_title);
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        
        return view($this->module_view_folder.'.index',$this->arr_view_data);
    }

    public function show($enc_id)
    {
        $id = base64_decode($enc_id);

        $arr_data = array();

        $obj_data = $this->BaseModel->where('id',$id)->first();
        
        if($obj_data != FALSE)
        {
          $arr_data = $obj_data->toArray();

        }

        $this->arr_view_data['arr_data'] = $arr_data;

        $this->arr_view_data['page_title']      = "View ".str_singular($this->module_title);;
        $this->arr_view_data['module_title']    = str_plural($this->module_title);
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view($this->module_view_folder.'.show',$this->arr_view_data);
    }
    
    public function create()
    {
        $this->arr_view_data['arr_lang'] = $this->LanguageService->get_all_language();  

        $this->arr_view_data['page_title']      = "Create ".str_singular($this->module_title);
        $this->arr_view_data['module_title']    = str_plural($this->module_title);
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        

        return view($this->module_view_folder.'.create',$this->arr_view_data);
    }

    public function edit($enc_id)
    {
        $id = base64_decode($enc_id); 

        $obj_data = $this->BaseModel->where('id', $id)->with(['translations'])->first();

        $arr_data = [];
        if($obj_data)
        {
           $arr_data = $obj_data->toArray(); 

           /* Arrange Locale Wise */
           $arr_data['translations'] = $this->arrange_locale_wise($arr_data['translations']);
        }

        $this->arr_view_data['edit_mode'] = TRUE;
        $this->arr_view_data['enc_id']    = $enc_id;
        $this->arr_view_data['arr_lang']  = $this->LanguageService->get_all_language();
        
        $this->arr_view_data['arr_data']        = $arr_data;
        $this->arr_view_data['page_title']      = "Edit ".str_singular($this->module_title);
        $this->arr_view_data['module_title']    = str_plural($this->module_title);
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view($this->module_view_folder.'.edit',$this->arr_view_data);    
    }

    public function save(Request $request)
    {
        $is_update = false;

        $form_data = $request->all();

        $country_id = base64_decode($request->input('enc_id'),false);

        $country_id = $country_id=="" ? false : $country_id;
            
        if($request->has('enc_id'))
        {
            $is_update = true;
        }

         /*Check Validations*/
        $arr_rules = [
                        'country_code'    =>'required|max:3',
                        'country_name_en' =>'required'
                     ];

        $validator = Validator::make($request->all(),$arr_rules,[
                                                                  'country_code.required'    =>   'Enter Country Code',
                                                                  'country_name_en.required' =>   'Enter Country Name',
                                                                ]);
        if($validator->fails())
        {
           $response['status'] = 'warning';
           $response['description'] = 'Form Validation Failed Please CheckForm Fields.';

           return response()->json($response);
        }
        /* Check if country already exists with given translation */
        $is_duplicate = $this->BaseModel
                            ->whereHas('translations',function($query) use($request)
                                {
                                    $query->where('locale','en')
                                          ->where('country_name',$request->input('country_name_en'));      
                                });
                            
        if($is_update)
        {
            $is_duplicate = $is_duplicate->where('id','<>',$country_id);
        }

        $does_exists = $is_duplicate->count();

        if($does_exists)
        {
            $response['status'] = 'warning';
            $response['description'] = str_singular($this->module_title).' Already Exists.';

            return response()->json($response);
        }

        /* Insert into Country Table */

        $country = $this->CountryModel->firstOrNew(['id' => $country_id]);

        $country->country_code =  $form_data['country_code'];

        $country_details = $country->save();

        if($country_details)      
        {
            $arr_lang =  $this->LanguageService->get_all_language();      
         
            /* insert record into translation table */
            if(sizeof($arr_lang) > 0 )
            {
                foreach ($arr_lang as $lang) 
                {   
                
                    $arr_data = array();
                    $country_name = $form_data['country_name_'.$lang['locale']];

                    if( isset($country_name) && $country_name != '')
                    { 
                        $translation = $country->getTranslation($lang['locale']);   

                        if($translation)
                        {
                            $translation->country_name =  $country_name;
                            $translation->save();    
                        }  
                        else
                        {
                            /* Create New Language Entry  */
                            $translation = $country->getNewTranslation($lang['locale']);
                            $translation->country_id   =  $country->id;
                            $translation->country_name =  $country_name;
                            $translation->country_slug = str_slug($form_data['country_name_en'], '-');

                            $translation->save();
                        } 
                    }
                }//foreach 
            } //ifs

            /*-------------------------------------------------------
            |   Activity log Event
            --------------------------------------------------------*/
                $arr_event                 = [];
                $arr_event['ACTION']       = 'ADD';

                if($is_update)
                {
                    $arr_event['ACTION']   = 'EDIT';
                }
                
                $arr_event['MODULE_TITLE'] = $this->module_title;

                $this->save_activity($arr_event);

               $response['status']      = 'success';
               $response['description'] = str_singular($this->module_title).' Save Successfully';

               if($is_update == false)
               {
                if($country->id)
                {
                    $response['link'] = url('/admin/countries/edit/'.base64_encode($country->id));
                }
               }

            /*----------------------------------------------------------------------*/
        }
        else
        {
               $response['status']      = 'error';
               $response['description'] = 'Error Occurred, While Save '.str_singular($this->module_title);
        }
              return response()->json($response);
    }

     public function delete($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            return redirect()->back();
        }

        if($this->perform_delete(base64_decode($enc_id)))
        {
            Flash::success(str_singular($this->module_title).' Deleted Successfully');
        }
        else
        {
            Flash::error('Problem Occured While '.str_singular($this->module_title).' Deletion ');
        }

        return redirect()->back();
    }
    
    
    public function multi_action(Request $request)
    {
        $arr_rules = array();
        $arr_rules['multi_action'] = "required";
        $arr_rules['checked_record'] = "required";


        $validator = Validator::make($request->all(),$arr_rules);

        if($validator->fails())
        {
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $multi_action = $request->input('multi_action');
        $checked_record = $request->input('checked_record');

        /* Check if array is supplied*/
        if(is_array($checked_record) && sizeof($checked_record)<=0)
        {
            Session::flash('error','Problem Occured, While Doing Multi Action');
            return redirect()->back();

        }

        
        foreach ($checked_record as $key => $record_id) 
        {  
            if($multi_action=="delete")
            {
               $this->perform_delete(base64_decode($record_id));    
               Flash::success(str_plural($this->module_title).' Deleted Successfully'); 
            } 
            elseif($multi_action=="activate")
            {
               $this->perform_activate(base64_decode($record_id)); 
               Flash::success(str_plural($this->module_title).' Activated Successfully'); 
            }
            elseif($multi_action=="deactivate")
            {
               $this->perform_deactivate(base64_decode($record_id));    
               Flash::success(str_plural($this->module_title).' Blocked Successfully');  
            }
        }

        return redirect()->back();
    }

    public function activate(Request $request)
    {
        $enc_id = $request->input('id');

        if(!$enc_id)
        {
            return redirect()->back();
        }

        $arr_response = [];    
        if($this->perform_activate(base64_decode($enc_id)))
        {
            $arr_response['status'] = 'SUCCESS';
        }
        else
        {
            $arr_response['status'] = 'ERROR';
        }

        $arr_response['data'] = 'ACTIVE';

        return response()->json($arr_response);
    }

    public function deactivate(Request $request)
    {
        $enc_id = $request->input('id');

        if(!$enc_id)
        {
            return redirect()->back();
        }
        $arr_response = []; 
        if($this->perform_deactivate(base64_decode($enc_id)))
        {
             $arr_response['status'] = 'SUCCESS';
        }
        else
        {
            $arr_response['status'] = 'ERROR';
        }

        $arr_response['data'] = 'DEACTIVE';

        return response()->json($arr_response);
    }

    public function perform_activate($id)
    {
        $activate = $this->BaseModel->where('id',$id)->update(['is_active'=>'1']);
        
        if($activate)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function perform_deactivate($id)
    {
        $deactivate     = $this->BaseModel->where('id',$id)->update(['is_active'=>'0']);
        
        if($deactivate)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function arrange_locale_wise(array $arr_data)
    {
        if(sizeof($arr_data)>0)
        {
            foreach ($arr_data as $key => $data) 
            {
                $arr_tmp = $data;
                unset($arr_data[$key]);

                $arr_data[$data['locale']] = $data;                    
            }

            return $arr_data;
        }
        else
        {
            return [];
        }
    }
    
    public function insert_records()
    {
        $country_obj = $this->CountryModel->get();

        if($country_obj)
        {
            $country_arr = $country_obj->toArray();
        }
        // $count = count($country_arr);
        foreach($country_arr as $k => $v)
        {
          $data['id']           = $v['id'];
          $data['country_name'] = $v['country_name'];
          $data['country_id']   = $v['id'];
          $data['country_slug'] = str_slug($v['country_name'], '-');
          $data['locale']       = 'en';

          $insert_countries = $this->CountryTranslationModel->create($data);
        }

        $CountryTranslationModel = $this->CountryTranslationModel->get();

        
        if($insert_countries)
        {
            $insert_countries;
        }
        else
        {
            dd(44545444);
        }
    }
}

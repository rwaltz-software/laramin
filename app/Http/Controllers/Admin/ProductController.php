<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductModel;
use App\Common\Traits\MultiActionTrait;
use Validator;
use Session;
use Sentinel;
use Flash;
use Activation;

class ProductController extends Controller
{
    //
    use MultiActionTrait;

    public function __construct(
                                ProductModel $product                               
                                ) 
    {
    	 $this->product_base_img_path   = base_path().config('app.project.img_path.product');
        $this->product_public_img_path = url('/').config('app.project.img_path.product');
        $this->ProductModel          = $product;
         $this->BaseModel            = $this->ProductModel;
        $this->arr_view_data      = [];
        $this->admin_url_path     = url(config('app.project.admin_panel_slug'));

        $this->module_title       = "Admin Product";
        $this->module_view_folder = "admin.product";
        $this->module_url_path    = $this->admin_url_path."/product";
 
    }

    public function index()
    {    	
        $arr_product = array();

        $obj_product = $this->ProductModel->get();

        $this->arr_view_data['obj_product']       = $obj_product;
        $this->arr_view_data['page_title']      = "Manage ".str_singular( $this->module_title);
        $this->arr_view_data['module_title']    = str_plural($this->module_title);
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
         $this->arr_view_data['product_public_img_path'] = $this->product_public_img_path;
        
        return view($this->module_view_folder.'.index',$this->arr_view_data);

    }


  	public function create()
    {
        // $obj_role = Sentinel::getRoleRepository()->createModel()->whereIn('slug',['admin']);
        // $obj_role = $obj_role->orderBy('id','desc')->get(); 

        // if( $obj_role != FALSE)
        // {
        //     $arr_roles = $obj_role->toArray();
        // }

        // $this->arr_view_data['arr_roles']       = $arr_roles;
        $this->arr_view_data['page_title']      = "Create ".str_singular( $this->module_title);
        $this->arr_view_data['module_title']    = str_plural($this->module_title);
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        
        return view($this->module_view_folder.'.create',$this->arr_view_data);
    }



    public function save(Request $request)
    {
        /* Is Update/ Create Process */
        $is_update_process = false;

        $form_data = $request->all();
        $id = $request->input('id',false);

        $id = $id ==""   ? false : $id;

        if($request->has('id'))
        {
            $is_update_process = true; 
        }

        $arr_rules = [
            'name' => 'required',
            'description' => 'required',
        ];

        if($is_update_process == false)
        {
            $arr_rules['image'] = "required|image|mimes:jpg,jpeg,png";    
        }
       

        $validator = Validator::make($request->all(),$arr_rules,[
                'name.required' => 'Name is required',
                'description.required' => 'Image is required'
            ]);

        if($validator->fails())
        {
            $response['status'] = 'warning';
            $response['description'] = 'Valiation Failed Please check The All Fields In Form.';

            return response()->json($response);
        }

        // $insert_arr = array(
        // 						'name'=>$form_data['name'],
        // 						'description'=>$form_data['description']
        // 					);

        // $insert = $this->ProductModel->create($insert_arr);
        // if($insert)
        // {
        // 	$response['status'] = "success";
        // 	$response['description'] = "Product Inserted Successfully";
        // }
        // else
        // {
        // 	$response['status'] = "error";
        // 	$response['description'] = "Unable to Insert Product";	
        // }


        // return response()->json($response);
       

        /* Main Model Entry */
        $product = ProductModel::firstOrNew(['id' => $id]);

        /* Image Handling */

        $file_name = false;
        if($request->hasFile('image'))
        {
            $file_name = $form_data['image'];

            $file_extension = strtolower($request->file('image')->getClientOriginalExtension()); 

            if($is_update_process)
            {
            	unlink($this->product_base_img_path.$product->image);
                // $this->perform_product_image_unlink($product);    
            }

            $file_name = sha1(uniqid().$file_name.uniqid()).'.'.$file_extension;

            $request->file('image')->move($this->product_base_img_path, $file_name);  
        }

        $product->name = $form_data['name'];
        $product->description = $form_data['description'];

        if($is_update_process != false)
        {
            $product->status = 'ACTIVE'; 
        }

        if($file_name != false)
        {
            $product->image = $file_name;    
        }        

        $product->save();

        if($product)
        {
            $response['status'] = 'success';
            $response['description'] = str_singular($this->module_title).' Saved Successfully.';
            if($is_update_process == false)
             {
                if($product->id)
                {
                    $response['link'] = url('/admin/product/edit/'.base64_encode($product->id));
                }
             }
        }       
        else
        {
            $response['status'] = 'error';
            $response['description'] = 'Error Occurred while Save'.str_singular($this->module_title).' Please Try Again.!';
            // return redirect()->back()->withInput($request->all());
        }
        return response()->json($response);


    }



    public function edit($enc_id)
    {

        $id = base64_decode($enc_id);

        $data     = array();

        $obj_data = $this->ProductModel->where('id', $id)->first();

        $arr_product = [];
        if($obj_data)
        {
           $arr_product = $obj_data->toArray(); 
        }


        $product_public_img_path = $this->product_public_img_path;

        $this->arr_view_data['edit_mode']                = TRUE;
        $this->arr_view_data['enc_id']                   = $enc_id;
        $this->arr_view_data['product_public_img_path'] = $product_public_img_path;
        $this->arr_view_data['module_url_path']          = $this->module_url_path;
     
       	$this->arr_view_data['arr_product'] = $arr_product;  

        $this->arr_view_data['page_title']   = "Edit ".str_singular($this->module_title);
        $this->arr_view_data['module_title'] = str_singular($this->module_title);

        return view($this->module_view_folder.'.edit',$this->arr_view_data);   
    }



    public function activate(Request $request)
    {
        $enc_id = $request->input('id');

        if(!$enc_id)
        {
            return redirect()->back();
        }

        $arr_response = [];    
        if($this->perform_activate(base64_decode($enc_id)))
        {
            $arr_response['status'] = 'SUCCESS';
        }
        else
        {
            $arr_response['status'] = 'ERROR';
        }

        $arr_response['data'] = 'ACTIVE';

        return response()->json($arr_response);
    }

    public function deactivate(Request $request)
    {
        $enc_id = $request->input('id');

        if(!$enc_id)
        {
            return redirect()->back();
        }
        $arr_response = []; 
        if($this->perform_deactivate(base64_decode($enc_id)))
        {
             $arr_response['status'] = 'SUCCESS';
        }
        else
        {
            $arr_response['status'] = 'ERROR';
        }

        $arr_response['data'] = 'DEACTIVE';

        return response()->json($arr_response);
    }

    public function perform_activate($id)
    {
        $product = $this->ProductModel->where('id',$id)->update(['status'=>'ACTIVE']);       
        if($product)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function perform_deactivate($id)
    {
        $product     = $this->ProductModel->where('id',$id)->update(['status'=>'BLOCK']);       
        if($product)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
}

<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\CityModel;
use App\Models\CountryModel;
use App\Models\StateModel;
use App\Models\CityTranslationModel;
use Illuminate\Http\Request;
use Datatables;
use Session;
use Validator;
use App\Common\Services\LanguageService;
use App\Common\Traits\MultiActionTrait;
use Flash;
 
class CityController extends Controller
{
    use MultiActionTrait;
    public function __construct(CountryModel $countries,
                                StateModel $state,
                                CityModel $city,
                                LanguageService $langauge,
                                CityTranslationModel $CityTranslationModel
                                )
    {
        $this->CountryModel       = $countries;
        $this->StateModel         = $state;
        $this->CityModel          = $city;
        $this->LanguageService    = $langauge;
        
        $this->BaseModel          = $this->CityModel;
        $this->CityTranslationModel = $CityTranslationModel;

        $this->arr_view_data      = [];
        $this->module_url_path    = url(config('app.project.admin_panel_slug')."/cities");
        $this->module_title       = "Cities";
        $this->module_url_slug    = "cities";
        $this->module_view_folder = "admin.cities";

    }

    public function index()
    {
        $arr_view_data['module_url_path'] = $this->module_url_path;
        $arr_view_data['module_title'] = $this->module_title;
        return view($this->module_view_folder.'.index',$arr_view_data);
    }

    public function get_city($country_id = false)
    {
       $city_details = $this->CityModel->get();

       $city_details = $this->CityModel->with(['state_details'=>function($query){ 

                                                    return $query->select('name');
                                                }]);  

       return Datatables::of($city_details)->make(true);
    }

    public function show($enc_id)
    {
        $id = base64_decode($enc_id);
        $arr_data = array();

        $obj_data = $this->BaseModel->where('id',$id)->with(['country_details','state_details'])->first();
        if( $obj_data != FALSE)
        {
            $arr_data = $obj_data->toArray();
        }

        $this->arr_view_data['arr_data']        = $arr_data;
        $this->arr_view_data['page_title']      = "Show ".str_singular($this->module_title);
        $this->arr_view_data['module_title']    = str_plural($this->module_title);
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        
        return view($this->module_view_folder.'.show', $this->arr_view_data);
    }
    
    public function create()
    {
        $arr_country = array();

        /* Build Country Module */
        $obj_country = $this->CountryModel->where('is_active','1')->get();

        if( $obj_country != FALSE)
        {
            $arr_country = $obj_country->toArray();
        }

        $arr_default = [0 => "-- Select --"];

        $this->arr_view_data['arr_country'] = $this->build_select_options_array($arr_country,'id','country_name',$arr_default);

        /* Build State Module */
        $obj_state    = $this->StateModel->where('is_active','1')->get();
        if( $obj_state != FALSE)
        {
            $arr_state = $obj_state->toArray();
        }
       
        $this->arr_view_data['arr_state'] = $this->build_select_options_array($arr_state,'id','state_title',$arr_default);

        $this->arr_view_data['arr_lang']        = $this->LanguageService->get_all_language();
        $this->arr_view_data['page_title']      = "Create ".str_singular($this->module_title);
        $this->arr_view_data['module_title']    = str_plural($this->module_title);
        $this->arr_view_data['module_url_path'] = $this->module_url_path;
        
        return view($this->module_view_folder.'.create', $this->arr_view_data);
    }


    public function edit($enc_id)
    {
        $id = base64_decode($enc_id);

        $this->arr_view_data['states'] = $this->arr_view_data['states'] = $this->StateModel->get();

        // $this->arr_view_data['countries']=$this->arr_view_data['country'] = $this->CountryModel->get();

         $arr_country = array();

        /* Build Country Module */
        $obj_country = $this->CountryModel->where('is_active','1')->get();

        if( $obj_country != FALSE)
        {
            $arr_country = $obj_country->toArray();
        }

        $arr_default = [0 => "Select"];

        $this->arr_view_data['arr_country'] = $this->build_select_options_array($arr_country,'id','country_name',$arr_default);


        $arr_data = array();

        $obj_data = $this->BaseModel
                           ->where('id',$id)
                           ->with(['country_details','state_details','translations'])
                           ->first();

        if($obj_data)
        {
           $arr_data = $obj_data->toArray();
           /* Arrange Locale Wise */
           $arr_data['translations'] = $this->arrange_locale_wise($arr_data['translations']);
        }
       
        $arr_lang =  $this->LanguageService->get_all_language();


        $this->arr_view_data['edit_mode']       = TRUE;
        $this->arr_view_data['enc_id']          = $enc_id;
        $this->arr_view_data['arr_lang']        = $this->LanguageService->get_all_language();
        $this->arr_view_data['arr_data']        = $arr_data;
        $this->arr_view_data['page_title']      = "Edit ".str_singular($this->module_title);
        $this->arr_view_data['module_title']    = str_plural($this->module_title);
        $this->arr_view_data['module_url_path'] = $this->module_url_path;

        return view($this->module_view_folder.'.edit', $this->arr_view_data);    
    }


    public function save(Request $request)
    {
        $is_update = false;

        $form_data = $request->all();

        $city_id = base64_decode($request->input('city_id',false));

        $city_id = $city_id ==""  ? false : $city_id;

        if($request->has('city_id'))
        {
            $is_update = true;
        }

        $arr_rules= [
                        'country_id'=>'required',
                        'state_id'=>'required',
                        'city_title_en'=>'required'
                    ];

        $validator = Validator::make($request->all(),$arr_rules,[
                         'country_id.required'    =>  'Please Select Country.', 
                         'state_id.required'      =>  'Please Select State.', 
                         'city_title_en.required' =>  'Enter City Name.',
                      ]);
        
        if($validator->fails())
        {
            // return redirect()->back()
            //                 ->withInput($request->all())
            //                 ->withErrors($validator);

            $response['status'] = 'warning';
            $response['description'] = 'Form Validation Failed Please Check Form Field';

            return response()->json($response);                
        }

        $is_duplicate = $this->BaseModel->where('state_id', $request->input('state_id'))
                            ->whereHas('translations',function($query) use($request)
                            {
                                $query->where('locale', 'en')
                                      ->where('city_title',$request->input('city_title_en'));
                            });

       if($is_update)
        {
            $is_duplicate= $is_duplicate->where('id','<>',$city_id);
        }

        $does_exists = $is_duplicate->count();

        if($does_exists)
        {
            // Flash::error(str_singular($this->module_title).' Already Exists.');
            // return redirect()->back()->withInput()->with($request->all());
            $response['status'] = 'warning';
            $response['description'] = 'Form Validation Failed Plaese Check Form Fields.';

            return response()->json($response);
        }

        $city_data =  $this->CityModel->firstOrNew(['id' => $city_id]);

        // $city_data->public_key   = str_random(7);
        $city_data->country_id   =  $form_data['country_id'];
        $city_data->state_id     =  $form_data['state_id'];
        $city_data->name         = $form_data['city_title_en'];
        $city_details            = $city_data->save();

        if($city_data)
         {  
            $arr_lang =  $this->LanguageService->get_all_language();

            if(sizeof($arr_lang) > 0 )
            {
                foreach ($arr_lang as $lang) 
                {   
                    $arr_data = array();

                    $city_title = $request->input('city_title_'.$lang['locale']);

                    if( isset($city_title) && $city_title != "" )
                    { 
                       $translation = $city_data->translateOrNew($lang['locale']);
                       $translation->city_id     = $city_data->id;

                       if($is_update != false)
                       {
                         $translation = $city_data->getTranslation($lang['locale']);   
                       }

                       if($translation)
                       {
                            $translation->city_title  = $city_title;
                            $translation->city_slug   = str_slug($form_data['city_title_en'], "-");
                            $translation->save();
                       }
                       else
                       {
                            /* Create New Language Entry  */
                            $translation = $city_data->getNewTranslation($lang['locale']);
                            
                            $translation->city_id    =  $city_id;
                            $translation->city_title =  $city_title;
                            $translation->city_slug  =  str_slug($form_data['city_title_en'], "-");
                            $translation->save();    
                        } 
                        // Flash::success(str_singular($this->module_title).' Save Successfully');  
                    }
                }//foreach
             } //if
            
              /*-------------------------------------------------------
            |   Activity log Event
            --------------------------------------------------------*/
                $arr_event                 = [];
                $arr_event['ACTION']       = 'ADD';

                if($is_update)
                {
                    $arr_event['ACTION']       = 'EDIT';
                }
                
                $arr_event['MODULE_TITLE'] = $this->module_title;

                $this->save_activity($arr_event);

                $response['status'] = 'success';
                $response['description'] = str_singular($this->module_title).' Save Successfully.';

                if($is_update == false)
                {
                    if($city_data->id)
                    {
                        $response['link'] = url('/admin/cities/edit/'.base64_encode($city_data->id));
                    }
                }

            /*----------------------------------------------------------------------*/
          }
         else
         {
             $response['status'] = 'error';
             $response['description'] = 'Error Occured While Save'.str_singular($this->module_title).'.';
         }
         
         return response()->json($response);
    }


    public function arrange_locale_wise(array $arr_data)
    {
        if(sizeof($arr_data)>0)
        {
            foreach ($arr_data as $key => $data) 
            {
                $arr_tmp = $data;
                unset($arr_data[$key]);

                $arr_data[$data['locale']] = $data;                    
            }

            return $arr_data;
        }
        else
        {
            return [];
        }
    }
    
    public function activate(Request $request)
    {
        $enc_id = $request->input('id');

        if(!$enc_id)
        {
            return redirect()->back();
        }

        $arr_response = [];    
        if($this->perform_activate(base64_decode($enc_id)))
        {
            $arr_response['status'] = 'SUCCESS';
        }
        else
        {
            $arr_response['status'] = 'ERROR';
        }

        $arr_response['data'] = 'ACTIVE';

        return response()->json($arr_response);
    }

    public function deactivate(Request $request)
    {
        $enc_id = $request->input('id');

        if(!$enc_id)
        {
            return redirect()->back();
        }
        $arr_response = []; 
        if($this->perform_deactivate(base64_decode($enc_id)))
        {
             $arr_response['status'] = 'SUCCESS';
        }
        else
        {
            $arr_response['status'] = 'ERROR';
        }

        $arr_response['data'] = 'DEACTIVE';

        return response()->json($arr_response);
    }

    public function perform_activate($id)
    {
        $activate = $this->BaseModel->where('id',$id)->update(['is_active'=>'1']);
        
        if($activate)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }

    public function perform_deactivate($id)
    {
        $deactivate     = $this->BaseModel->where('id',$id)->update(['is_active'=>'0']);
        
        if($deactivate)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
   
    public function delete($enc_id = FALSE)
    {
        if(!$enc_id)
        {
            return redirect()->back();
        }

        if($this->perform_delete(base64_decode($enc_id)))
        {
            Flash::success(str_singular($this->module_title).' Deleted Successfully');
        }
        else
        {
            Flash::error('Problem Occured While '.str_singular($this->module_title).' Deletion ');
        }

        return redirect()->back();
    }

    public function perform_delete($id)
    {
        $entity = $this->BaseModel->where('id',$id)->first();
        if($entity)
        {
            return $entity->delete();
        }

        return FALSE;
    }

    public function build_select_options_array(array $arr_data,$option_key,$option_value,array $arr_default)
    {

        $arr_options = [];
        if(sizeof($arr_default)>0)
        {
            $arr_options =  $arr_default;   
        }

        if(sizeof($arr_data)>0)
        {
            foreach ($arr_data as $key => $data) 
            {
                if(isset($data[$option_key]) && isset($data[$option_value]))
                {
                    $arr_options[$data[$option_key]] = $data[$option_value];
                }
            }
        }

        return $arr_options;
    }

    public function insert_records()
    {
        $city_obj = $this->CityModel->limit(1000)->get();

        if($city_obj)
        {
            $city_arr = $city_obj->toArray();
        }
        // $count = count($country_arr);
        foreach($city_arr as $k => $v)
        {
          $data['city_id']      = $v['id'];
          $data['city_title']   = $v['name'];
          $data['city_slug']    = str_slug($v['name'], '-');
          $data['locale']       = 'en';

          $insert_city = $this->CityTranslationModel->create($data);
        }

              
        if($insert_city)
        {
            $insert_countries;
        }
        else
        {
            dd(44545444);
        }
    }
}
